-module(parse_test).

-compile(export_all).
-include_lib("eunit/include/eunit.hrl").

%% parse the binary readings to a list of
%% {data, SensorCode, SensorNum, DT_Code, Value / Scalar}
%% For each sensor value, retrieve the sensor scalar with get_data_type 
%% and scale the value accordingly
parse(<<SensorCode:32/integer-little, Readings/binary>>) ->
    parse_readings(Readings, SensorCode, []).
 
parse_readings(<<>>, _, _) ->
    [];
parse_readings(<<DT_Code:8, Rest/binary>>, SensorCode, SensorCount) -> 
    {ByteSize, Scalar} = get_data_type(DT_Code),                         %% changed
    io:format("DT_Code: ~w, ByteSize: ~w, Scalar ~w", [DT_Code, ByteSize, Scalar]),
    Bits = ByteSize * 8, 
    <<Value:Bits/integer-little, Rest2/binary>> = Rest,
    io:format("Value: ~w, Scalar: ~w",[Value, Scalar]),
    SensorNum  = sensor_count(DT_Code, SensorCount),
    [{data, SensorCode, SensorNum, DT_Code, Value / Scalar}] ++ 
        parse_readings(Rest2, SensorCode, inc_sensor_count(DT_Code, SensorCount)).

%sensor_count(DT_Code, []) -> {1, inc_sensor(DT_Code, [])};
sensor_count(DT_Code, PropL) ->
    case proplists:lookup(DT_Code, PropL) of
        {DT_Code, Count} -> Count+1;
        none             -> 1
    end.

%inc_sensor(DT_Code, []) -> [{DT_Code, 1}];
inc_sensor_count(DT_Code, SensorCount) ->
    case lists:keyfind(DT_Code, 1, SensorCount) of
        false -> [{DT_Code, 1}|SensorCount];
        {DT_Code, Count} -> 
            lists:keyreplace(DT_Code, 1, SensorCount, {DT_Code, Count+1})
    end. 

%% gets {DT_Id, ByteSize, Scalar} from DT_Code (hardcoded for now)

%% #<DataType id: 3, data_type_code: 3, unit: "Pa", description: "Barometric pressure", created_at: "2013-09-30 05:44:10", updated_at: "2013-10-21 00:12:57", number_of_bytes: 4, scalar: 1>, 
%% #<DataType id: 1, data_type_code: 0, unit: "°C", description: "External temperature", created_at: "2013-09-30 05:44:10", updated_at: "2013-10-31 00:39:09", number_of_bytes: 2, scalar: 100>, 
%% #<DataType id: 2, data_type_code: 1, unit: "°C", description: "Internal temperature", created_at: "2013-09-30 05:44:10", updated_at: "2013-10-31 00:39:16", number_of_bytes: 2, scalar: 100>, 
%% #<DataType id: 5, data_type_code: 4, unit: "V", description: "Battery Voltage", created_at: "2013-10-21 00:15:41", updated_at: "2013-10-31 00:39:38", number_of_bytes: 2, scalar: 100>, 
%% #<DataType id: 4, data_type_code: 2, unit: "%RH", description: "Relative Humidity", created_at: "2013-10-21 00:12:14", updated_at: "2013-10-31 00:39:47", number_of_bytes: 2, scalar: 100>
get_data_type(0) -> {2, 100};
get_data_type(1) -> {2, 100};
get_data_type(2) -> {2, 100};
get_data_type(3) -> {4, 1};
get_data_type(4) -> {2, 100}.




t1_test_() ->
    Bytes = <<1, 0, 0, 0, 
              0,       %% a temperature
              255, 0>>, %% little endian
    [?_assert(parse(Bytes) == [{data, 1, 1, 0, 2.55}])].


% {data, SensorCode, SensorNum, DT_Code, Value / Scalar}
t2_test_() ->
    Bytes = <<1, 0, 0, 0, 
              0,       %% a temperature
              255, 0,   %% little endian
              0,       %% another temperature
              0, 1,
              4,       %% voltage 
              10, 0
            >>, 
    [?_assert(parse(Bytes) == [{data, 1, 1, 0, 2.55},
                               {data, 1, 2, 0, 2.56},
                               {data, 1, 1, 4, 0.1}
                              ])].
