-module(modem_test).

-compile(export_all).

-include_lib("eunit/include/eunit.hrl").

-define(M, enigma_router_modem).

setup() ->
    {ok, Pid} = ?M:start_link(),
%    dbg:tracer(),
%    dbg:p(Pid,[m]),
    ?debugFmt("Modem pid:~w~n", [Pid]),
    Pid.

cleanup(Pid) ->
    ?M:stop(Pid).

modem_test_() ->
    {setup,
     fun setup/0,
     fun cleanup/1,
     fun tests/1
    }.

tests(Pid) ->
    Serial = ?M:serial(Pid),
    {inorder, %% ensure that these run in order
     [
      ?_assertEqual(?M:check_uart(Serial), [<<"OK">>]),
      ?_assert(is_integer(?M:get_signal_strength(Serial))),
      ?_assertEqual(?M:check_signal_strength(
                       ?M:get_signal_strength(Serial), 5), ok),
      ?_assertEqual(?M:show_network_registration(Serial), [<<"OK">>]),
      ?_assertEqual(?M:check_network_registration(Serial), 1),
      ?_assertEqual(?M:set_gprs_conn(Serial), [<<"OK">>]),
      ?_assertEqual(?M:connect_network(Serial),[<<"OK">>]),
      ?_assertEqual(?M:open_tcp(Serial),[<<"OK">>]),
      ?_assertEqual(?M:send_tcp_data(Serial,<<"Hello!">>),[<<"OK">>]),
      ?_assertEqual(?M:close_tcp(Serial),[<<"OK">>]),
      ?_assertEqual(?M:disconnect_network(Serial),[<<"OK">>]),
      ?_assertEqual(?M:echo_on(Serial),[<<"OK">>]),
      ?_assertEqual(?M:echo_off(Serial),[<<"OK">>]),
      extract_matches_test()
     ]}.


extract_matches_test() ->
    Res = ?M:extract_matches(
           [{10,3},{14,2},{17,2},{20,2}],
           <<"\r\n+AIPA:1,208,45,67,89\r\n\r\nOK\r\nother stuff">>),
        ?_assertEqual(Res,[<<"208">>,<<"45">>,<<"67">>,<<"89">>]).
  


