-module(xbee_test).

-compile(export_all).

-include_lib("eunit/include/eunit.hrl").


setup() ->
    {ok, Pid} = enigma_router_xbee:start_link(),
%    dbg:tracer(),
%    dbg:p(Pid,[m]),
    ?debugFmt("Xbee pid:~w~n", [Pid]),
    Pid.

cleanup(_) ->
    ok.

xbee_test_() ->
    {setup,
     fun setup/0,
     fun cleanup/1,
     fun tests/1
    }.

tests(Pid) ->
      [
       %% these parsing tests require human-checking in the Erlang console
       ?_assert(test_parsing(Pid) =:= ok),
       ?_assert(test_parsing(Pid) =:= ok),
       ?_assert(test_parsing(Pid) =:= ok),
       ?_assert(test_parsing(Pid) =:= ok),

       ?_assert(test_checksum())
      ].
  
%%-------------------------------------------------------------

test_packet() ->
    <<16#7E, 16#00, 16#16, 
      16#10, 16#01, 16#00, 16#13, 16#A2, 16#00, 16#40, 16#0A, 
      16#01, 16#27, 16#FF, 16#FE, 16#00, 16#00, 16#54, 16#78, 
      16#44, 16#61, 16#74, 16#61, 16#30, 16#41,
      16#13>>.

test_frame() ->
    <<16#10, 16#01, 16#00, 16#13, 16#A2, 16#00, 16#40, 16#0A, 
      16#01, 16#27, 16#FF, 16#FE, 16#00, 16#00, 16#54, 16#78, 
      16#44, 16#61, 16#74, 16#61, 16#30, 16#41>>.

test_parsing(Pid) -> 
    ?debugFmt("************************************************~n",[]),
    Msg = test_packet(),
    Length = byte_size(Msg),
    Parts = random:uniform(Length),
    test_parsing_loop(Parts, Msg, Pid).

test_parsing_loop(0, _, _) ->
    ok;
test_parsing_loop(_, <<>>, _) ->
    ok;
test_parsing_loop(N, Bytes, Pid) ->
    Delay = random:uniform(500), %% msec
    timer:sleep(Delay),
    NbBytes = random:uniform(byte_size(Bytes)),
    Bytes1 = binary:part(Bytes, 0, NbBytes),
    Bytes2 = binary:part(Bytes, NbBytes, byte_size(Bytes) - NbBytes),
    ?debugFmt("Sending ~p to ~p~n",[{data, Bytes1}, Pid]),
    Pid ! {data, Bytes1},
    test_parsing_loop(N-1, Bytes2, Pid).


test_checksum() ->
    enigma_router_xbee:check(16#13, test_frame()).


