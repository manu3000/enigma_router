-module(alert_test).

-compile(export_all).
-include_lib("eunit/include/eunit.hrl").

%% ONTEMP=0, OFFTEMP=2, ONDELAY=120, OFFDELAY=300
-define(ALERT_CONF,{0,2,120,300}).

t1_test_() ->
    %% get_status(DT=0,Temp,Status,Since,Now,{OnTemp,OffTemp,OnDelay,OffDelay})

    %Res=enigma_router_alert:get_status(0, 0.5, alert, {0,0,0}, {0,60,0}, ?ALERT_CONF),
    %error_logger:info_msg("res: ~p~n",[Res]),

    [%% temp under 0C, when status = alert for over 120sec
     ?_assert(enigma_router_alert:get_status(0, -0.5, alert, {0,0,0}, {0,121,0}, ?ALERT_CONF)  == 
                  {alert, {0,0,0}, start_pump}),

     %% temp over 2C, when status = ok for over 300sec
     ?_assert(enigma_router_alert:get_status(0, 2.5, ok, {0,0,0}, {0,301,0}, ?ALERT_CONF) == 
                  {ok, {0,0,0}, stop_pump}),

     %% temp over 0C, when status = alert -> status stays the same
     ?_assert(enigma_router_alert:get_status(0, 0.5, alert, {0,0,0}, {0,60,0}, ?ALERT_CONF) == 
                  {alert, {0,0,0}, no_action}),

     %% temp under 0C, when status = ok
     ?_assert(enigma_router_alert:get_status(0, -0.5, ok, {0,0,0}, {0,60,0}, ?ALERT_CONF)  == 
                  {alert, {0,60,0}, no_action}),

     %% temp over 2C, when status = alert
     ?_assert(enigma_router_alert:get_status(0, 2.1, alert, {0,0,0}, {0,60,0}, ?ALERT_CONF)  == 
                  {ok, {0,60,0}, no_action})

    ].







%% error_logger:info_msg("res: ~p~n",[Res]),
