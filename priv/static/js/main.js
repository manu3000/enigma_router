// --- Websocket stuff -----------------------------------------

var websocket;
var chart;

$(document).ready(init);

function isLocalIP() {
    if (window.XMLHttpRequest) xmlhttp = new XMLHttpRequest();
    else xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

    xmlhttp.open("GET"," http://api.hostip.info/get_html.php ",false);
    xmlhttp.send();

    hostipInfo = xmlhttp.responseText.split("\n");

    for (i=0; hostipInfo.length >= i; i++) {
        ipAddress = hostipInfo[i].split(":");
        if ( ipAddress[0] == "IP" ) {
		if (ipAddress[1] == "139.80.88.228")
		{ 
			return true;
		} else {
			return false;
		}
	}
    }
    return false;
}


function init() {
    if(!("WebSocket" in window)){  
        $('#status').append('<p><span style="color: red;">websockets are not supported </span></p>');
        $("#navigation").hide();  
    } else {
        $('#status').append('<p><span style="color: green;">websockets are supported </span></p>');
        connect();
    };
};

function connect()
{
    try {
        if (isLocalIP()) {
            wsHost = "ws://localhost:8888/websocket";
        } else {
            wsHost = "ws://139.80.88.228:8888/websocket";
        }
    }
    catch(err) {
        wsHost = "ws://localhost:8888/websocket";
    }

    websocket = new WebSocket(wsHost);
    showScreen('<b>Connecting to: ' +  wsHost + '</b>'); 
    websocket.onopen = function(evt) { onOpen(evt) }; 
    websocket.onclose = function(evt) { onClose(evt) }; 
    websocket.onmessage = function(evt) { onMessage(evt) }; 
    websocket.onerror = function(evt) { onError(evt) }; 

};  

function disconnect() {
    websocket.close();
}; 

function toggle_connection(){
    if(websocket.readyState == websocket.OPEN){
        disconnect();
    } else {
        connect();
    };
};

function sendTxt(txt) {
    if(websocket.readyState == websocket.OPEN){
        websocket.send(txt);
        showScreen('sending: ' + txt); 
    } else {
        showScreen('websocket is not connected'); 
    };
};

function onOpen(evt) { 
    showScreen('CONNECTED'); 
    getStations();
};  

function onClose(evt) { 
    showScreen('DISCONNECTED');
};  

function getStations()
{
    $('#msg').text('Station list requested');
    sendTxt("\"stations\"");
}

function getRouterStatus()
{
	$('#router_status').html('XX');	
	$('#msg').text('Router status requested');
	sendTxt("\"router_status\"");
}

function getSignalStrength()
{
	$('#signal_strength').html('XX');	
	$('#msg').text('Signal strength requested');
	sendTxt("\"signal_strength\"");
}

function getBattery() {	

	if (chart != null)
	{
		chart.showLoading();
	}
	var sid = parseInt($('#stations').val());
	if (!isNaN(sid))
	{
		var req = {sensordata: {sensor_id: sid, data_type: 4}};
	    var json = JSON.stringify(req);
	    sendTxt(json);
	}	
}  

function getTemperatures() {	

	if (chart != null)
        {
		chart.showLoading();
        }
    var sid = parseInt($('#stations').val());
    if (!isNaN(sid))
    {
        var req = {sensordata: {sensor_id: sid, data_type: 0}};
        var json = JSON.stringify(req);
        sendTxt(json);
    }
    //sendTxt("\"sensordata\"");
    //sendTxt("\"etemps\"");
}

function getRH() {	
        if (chart != null)
	{
	    chart.showLoading();
	}
	var sid = parseInt($('#stations').val());
	if (!isNaN(sid))
	{
	    var req = {sensordata: {sensor_id: sid, data_type: 2}};
	    var json = JSON.stringify(req);
	    sendTxt(json);
	}
}  

/* Gavin: this is where the incoming JSON is handled */
function onMessage(evt) { 
    //showScreen('RESPONSE: ' + evt.data); 
    var data = JSON.parse(evt.data);
    if(data.msg)
        displayMsg(data.msg);
    else if (data.stations)
        displayStations(data.stations)
    else if(data.config)
        displayConfig(data.config);
    // else if(data.etemps)
    // 	displayTemps(data.etemps)
    else if(data.etemp)
     	displayTemp(data.etemp)
    else if(data.sensordata) {
	if (chart != null)
	{
		chart.hideLoading();
	}
    	displaySensorData(data.type, data.sensordata)
    }
    else if(data.status)
    	displayStatus(data.status)
    else if(data.router_status)
    	displayRouterStatus(data.router_status)
    else if(data.signal)
    	displaySignal(data.signal)
    else
        throw "Unknown WS message"
};  

function onError(evt) {
    showScreen('ERROR: ' + evt.data);
};

function showScreen(txt) { 
    // console.log(txt);
     $('#msg').text(txt);
};

function displayStations(stations) {
    $('#stations')
        .find('option')
        .remove();

    $.each(stations, function(key, value) {
        $('#stations')
            .append($("<option></option>")
            .attr("value", value)
            .text(value)); 
    });
}

function displayRouterStatus(status) {
	var status_string = "";
	var modem_status = "";

	if (parseInt(status.power) == 1)
	{
		status_string += "Frost protection ENABLED.  ";	
	} else {
		status_string += "Frost protection IDLE.  ";
	}
	
	if (parseInt(status.running) == 1)
	{
		status_string += "The pump is ON.  ";
	} else {
		status_string += "The pump is OFF.  ";
	}

	if (parseInt(status.error) == 1)
	{
		status_string += "The pump has an ERROR.";
	} else {
		status_string += "The pump is OK.";
	}

	if (parseInt(status.modem) == 1)
	{
		modem_status = "The modem is READY";
	} else {
		modem_status = "The modem is OFF";
	}

	$('#router_status').html(status_string);	
	$('#modem_status').html(modem_status);
	$('#msg').text('Router status updated');
	
}

function displayTemp(temp) {
	$('#external_temperature').html(parseFloat(temp).toFixed(2));
	$('#msg').text('External temperature updated');
}

function displaySignal(signal) {
	var signalValue = parseInt(signal);
	if (signalValue == 99)
	{
		signal = "Unknown";
	}
	else if (signalValue == 31)
	{
		signal = "Excellent  (" + signal +"/31)";
	}
	else if (signalValue > 20)
	{
		signal = "Very good (" + signal +"/31)";
	}
	else if (signalValue > 10)
	{
		signal = "Good (" + signal + "/31)";
	}
	else if (signalValue > 5)
	{
		signal = "Average (" + signal + "/31)";
	}
	else if (signalValue > 1)
	{
		signal = "Weak (" + signal + "/31)";
	}
	else if (signalValue > 0)
	{
		signal = "Very weak (1/31)";
	}
	else {
		signal = "No signal (0/31)";
	}
	$('#signal_strength').html(signal);	
	$('#msg').text('Signal strength updated');
}

function displayStatus(status) {
	$('#external_temperature').html(status.external_tmp);	
}

function displayConfig(conf){
    $('#gprs_datarate_slow').val(conf.gprs_datarate_slow);
    $('#gprs_datarate_fast').val(conf.gprs_datarate_fast);
    $('#gprs_datarate_temp').val(conf.gprs_datarate_temp);
    $('#action_on_temp').val(conf.on_temp);
    $('#action_on_delay').val(conf.on_delay);
    $('#action_off_temp').val(conf.off_temp);
    $('#action_off_delay').val(conf.off_delay);        
}

function displayMsg(txt){
    $('#msg').text(txt);
}

function saveConfig(conf){
    var gprs_datarate_slow = $('#gprs_datarate_slow').val();
    var gprs_datarate_fast = $('#gprs_datarate_fast').val();
    var gprs_datarate_temp = $('#gprs_datarate_temp').val();
    var action_on_temp = parseFloat($('#action_on_temp').val());
    var action_on_delay = parseInt($('#action_on_delay').val());
    var action_off_temp = parseFloat($('#action_off_temp').val());
    var action_off_delay = parseInt($('#action_off_delay').val());
    var conf = {config: {
                    "gprs_datarate_slow": gprs_datarate_slow,
                    "gprs_datarate_fast": gprs_datarate_fast,
                    "gprs_datarate_temp": gprs_datarate_temp,
                    "on_temp": action_on_temp,
                    "on_delay": action_on_delay,
                    "off_temp": action_off_temp,
                    "off_delay": action_off_delay
                }};
    var json = JSON.stringify(conf);
    sendTxt(json);
}

/*
 * {"config":{
        "gprs_datarate_slow" : 1800, // when temp is >= "gprs_datarate_temp" send data every x seconds (30 min)
        "gprs_datarate_fast" : 300, // when temp is < "gprs_datarate_temp" send data every x seconds (5 min)
        "gprs_datarate_temp" : 5, // temperature in degrees
        "action_on_temp" : 0, // temperature at which point the frost prevention should turn on if we have remained below the temperature for "action_on_delay" seconds.
        "action_on_delay" : 120, // only turn on if we have been below "action_on_temp" for x seconds (2 min)
        "action_off_temp" : 2, // temperature at which point the frost prevention should turn off if we have remained above the temperature for "action_off_delay" seconds.
        "action_off_delay" : 300, // only turn off if we have been above "action_off_temp" for x seconds (5 min)
    }
}
 */


//--------------------------------------------------------------
function displaySensorData(data_type, sensordata){
	
	var series_data = new Array();
	var series_data2 = new Array();
	var series_title = "";
	var series_yaxis = "";
	var series_suffix = "";
	
	var nowtime = new Date();
	var offset = nowtime.getTimezoneOffset();
	for (var r = sensordata.length-1; r >= 0; r--) {
		var dataDate = new Date(Date.UTC(sensordata[r].time[0], sensordata[r].time[1]-1, sensordata[r].time[2], sensordata[r].time[3], sensordata[r].time[4], sensordata[r].time[5], 0));
		if (sensordata[r].sensor_num == 1)
		{
			series_data.push([dataDate.getTime(), sensordata[r].value]);
		} else {
			series_data2.push([dataDate.getTime(), sensordata[r].value]);
		}
	}

	series_data.sort(function(a, b){
		return a[0]-b[0]
	})

	series_data2.sort(function(a, b){
		return a[0]-b[0]
	})

	
	if (data_type == 0)
	{
		series_title = "Temperature";
		series_yaxis = 'Temperature (°C)';
		series_suffix = '°C';
	}
	else if (data_type == 2)
	{			
		series_title = "Relative humidity";
		series_yaxis = 'RH (%)';
		series_suffix = '%';
	}
	else
	{
		series_title = "Battery";		
		series_yaxis = 'Volts (V)';
		series_suffix = 'V';	
	}
	
	if (data_type == 0)
	{
		data = [{
				name:series_title,
				data: series_data
			},
			{
				name:series_title,
				data: series_data2
			}];
	} else {
		data = [{
				name: series_title,
				data: series_data
			}];
	}
	displaySeriesData(series_title, series_yaxis, series_suffix, data);
	
}

//--------------------------------------------------------------
function displayTemps(temps){
	
	var temp_data = new Array();
	var nowtime = new Date();
	for (var r = temps.length-1; r >= 0; r--) {
		var newDate = new Date( nowtime.getTime() - (temps[r].time * 60 * 60 * 1000) );
		temp_data.push([newDate.getTime(), temps[r].temp]);
	}
	
	data = [{
		name: 'External temperature',
		data: temp_data
	}];
	
	displaySeriesData(data);
	
}

function displaySeriesData(title, yaxis, suffix, data) {
	
	chart = new Highcharts.Chart({
	    chart: {
		renderTo: 'container',
		zoomType: 'xy',
		spacingRight: 20
	    },
            title: {
                text: title
            },            
            xAxis: {
                type: 'datetime',
            },
            yAxis: {
                title: {
                    text: yaxis
                },
		plotLines: [{
			value: 0,
			width: 1,
			color: '#000000'
		}]
            },
            tooltip: {
                valueSuffix: suffix
            },
            legend: {
                enabled: false
            },
	    plotOptions: {
		line: {
			marker: {
				enabled: false
			}
		}
	    },
            series: data
        });
}

