PROJECT = enigma_router

include erlang.mk


start:
	exec sudo erl +K true -pa ebin -pa deps/*/ebin -sname enigma_router -s lager -s enigma_router -config enigma_router
#	exec sudo erl -pa ebin -pa deps/*/ebin -sname enigma_router -s enigma_router -config enigma_router -boot start_sasl

detached:
	exec sudo erl +K true -pa ebin -pa deps/*/ebin -sname enigma_router -s lager -s enigma_router -config enigma_router -setcookie BARBENEEDSRASING -detached 

#	exec sudo erl -pa ebin -pa deps/*/ebin -sname enigma_router -s enigma_router -config enigma_router -setcookie BARBENEEDSRASING -detached
#	exec sudo erl -pa ebin -pa deps/*/ebin -sname enigma_router -s enigma_router -config enigma_router -detached
#-boot start_sasl -sasl sasl_error_logger \{file,\"./enigma_router.log\"\} -detached 

remote:
	erl -sname enigmatest -setcookie BARBENEEDSRASING -remsh enigma_router@raspberrypi
# then do: > [group_leader(group_leader(), Pid) || Pid <- processes()].
# to get console output

build-plt:
	dialyzer --build_plt --output_plt .$(PROJECT).plt \
		--apps kernel stdlib crypto

dialyzer: all
	dialyzer -r ebin --plt .enigma_router.plt  -Werror_handling -Wrace_conditions \
	-Wunmatched_returns

# old, needs attention
test_all: test_alert test_xbee test_modem

test_xbee: all
	(cd test; erl -make)
	erl -pa ebin -eval 'eunit:test(xbee_test,[verbose]), init:stop().'

test_modem: all
	(cd test; erl -make)
	erl -pa ebin -eval 'eunit:test(modem_test,[verbose])' -s init stop

test_alert: all
	(cd test; erl -make)
	erl -pa ebin -eval 'eunit:test(alert_test,[verbose]), init:stop().'

clean_test:
	rm ebin/*_test.beam

kill_vm:
	sudo kill `ps aux | grep beam | tr -s " " | cut -d " " -f 2 | head -n 1`
