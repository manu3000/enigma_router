%%%-------------------------------------------------------------------
%%% @author manu <manu@chleb>
%%% @copyright (C) 2013, manu
%%% @doc This module implements the xbee parser state machine
%%%  
%%% @end
%%% Created : 30 Jul 2013 by manu <manu@chleb>
%%%-------------------------------------------------------------------
-module(enigma_router_xbee).

-behaviour(gen_fsm).
-compile([{parse_transform, lager_transform}]).

%% API
-export([start_link/0]).

%% Tests
-export([check/2]).

%% gen_fsm callbacks
-export([init/1, start/2, length/2, payload/2, state_name/3, handle_event/3,
         handle_sync_event/4, handle_info/3, terminate/3, code_change/4]).

-define(SERVER, ?MODULE).
-define(DEVICE, "/dev/ttyUSB0").

-define(NO_RECEPTION, 5 * 60 * 1000). %% 5 mn
-define(TIMEOUT, 5 * 1000). %% 5 sec
-define(SENSOR_TIMEOUT, 10 * 60 * 1000). %% 10 mn
-define(MAX_NULLS, 20).
-record(state, {serial, buffer, length, timer, escaping, nulls, sensor_timer}).


%%%===================================================================
%%% API
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Creates a gen_fsm process which calls Module:init/1 to
%% initialize. To ensure a synchronized start-up procedure, this
%% function does not return until Module:init/1 has returned.
%%
%% @spec start_link() -> {ok, Pid} | ignore | {error, Error}
%% @end
%%--------------------------------------------------------------------
start_link() ->
    gen_fsm:start_link({local, ?SERVER}, ?MODULE, [], []).

%%%===================================================================
%%% gen_fsm callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Whenever a gen_fsm is started using gen_fsm:start/[3,4] or
%% gen_fsm:start_link/[3,4], this function is called by the new
%% process to initialize.
%%
%% @spec init(Args) -> {ok, StateName, State} |
%%                     {ok, StateName, State, Timeout} |
%%                     ignore |
%%                     {stop, StopReason}
%% @end
%%--------------------------------------------------------------------
init([]) ->
    Serial= serial:start([{speed,9600},{open, ?DEVICE}]),
    %% enigma_router_main:xbee_ready(),
    SensorTimer = erlang:start_timer(?SENSOR_TIMEOUT, self(), sensor),
    {ok, start, #state{serial=Serial, buffer= <<>>, length=undefined,
		      escaping=false, nulls=0, sensor_timer=SensorTimer}}. 

%%--------------------------------------------------------------------
%% @private
%% @doc
 
%% There should be one instance of this function for each possible
%% state name. Whenever a gen_fsm receives an event sent using
%% gen_fsm:send_event/2, the instance of this function with the same
%% name as the current state name StateName is called to handle
%% the event. It is also called if a timeout occurs.
%%
%% @spec state_name(Event, State) ->
%%                   {next_state, NextStateName, NextState} |
%%                   {next_state, NextStateName, NextState, Timeout} |
%%                   {stop, Reason, NewState}
%% @end
%%--------------------------------------------------------------------
-spec start({'data', binary()}, #state{}) -> {'next_state', atom(), #state{}}.
%% transition to 'length' when first byte is 126 (0x7E)
%% buffer is empty
%% if we received a strem of null bytes we restart
start({data, <<0>>}, #state{nulls=N}=S) when N < ?MAX_NULLS ->
    {next_state, start, S#state{nulls=N+1}};
start({data, <<0>>}, S) ->
    lager:info("XBEE: TOO MANY NULLS - RESTART~n"),
    {stop, null_bytes, S};
start({data, <<16#7E>>}, S) -> 
    %% lager:info("XBEE: START OF MSG~n"),
    Timer = erlang:start_timer(?TIMEOUT, self(), parse_timeout), 
    {next_state, length, S#state{buffer= <<>>, timer=Timer}};  
%% discard other messages
start(_Event, State) ->
    %lager:info("~w--~w~n",[Event,State]),
    %error("start: ~p~n",[Event]),
    {next_state, start, State}.


-spec length({'data', binary()}, #state{}) -> {'next_state', atom(), #state{}}.
%% when buffer is empty, next byte is MSB
length({data, MSB}, #state{buffer= <<>>}=S) ->
    {next_state, length, S#state{buffer=MSB}};
%% when buffer holds MSB, next byte is LSB,
%% we calculate length and transition to 'payload'
length({data, LSB}, #state{buffer=MSB}=S) -> 
    <<Length:16>> = <<MSB/binary, LSB/binary>>,
    %% lager:info("XBEE PAYLOAD: Length ~w~n",[Length]),
    {next_state, payload, S#state{length=Length, buffer= <<>>}};
%% discard other messages
length(_Event, State) ->
    %lager:info("~w -- ~w~n",[Event,State]),
    %error("length: ~p~n",[Event]),
    {next_state, length, State}.


-spec payload({'data', binary()}, #state{}) -> {'next_state', atom(), #state{}}.
%% If Buffer size == Length, next byte is the checksum
%% We check the checksum and send to enigma_router_main
payload({data, <<CheckSum>>}, #state{length=L, buffer=B, timer=T}=S) 
  when byte_size(B) == L ->
    _ = erlang:cancel_timer(T),
    %% lager:info("XBEE PAYLOAD: MSG COMPLETE ~w, checksum ~w~n", [B, CheckSum]),
    forward(check(CheckSum, B), B), %% check the checksum and send
    {next_state, start, 
     S#state{buffer= <<>>, length=undefined, timer=undefined}};
%    {next_state, payload, S#state{buffer= <<B/binary, <<CheckSum>>/binary>>}};
%% if Buffer size < Length, we add next byte to the buffer
payload({data, Byte}, #state{buffer=B}=S) ->
    %%lager:info("XBEE PAYLOAD: Buffering ~w~n",[<<B/binary, Byte/binary>>]),
    {next_state, payload, S#state{buffer= <<B/binary, Byte/binary>>}};
%% discard other messages
payload(_Event, State) ->
    %lager:info("~w -- ~w~n",[Event, State]),
    %error("payload: ~p~n",[Event]),
    {next_state, payload, State}.


%%--------------------------------------------------------------------
%% @private
%% @doc
%% There should be one instance of this function for each possible
%% state name. Whenever a gen_fsm receives an event sent using
%% gen_fsm:sync_send_event/[2,3], the instance of this function with
%% the same name as the current state name StateName is called to
%% handle the event.
%%
%% @spec state_name(Event, From, State) ->
%%                   {next_state, NextStateName, NextState} |
%%                   {next_state, NextStateName, NextState, Timeout} |
%%                   {reply, Reply, NextStateName, NextState} |
%%                   {reply, Reply, NextStateName, NextState, Timeout} |
%%                   {stop, Reason, NewState} |
%%                   {stop, Reason, Reply, NewState}
%% @end
%%--------------------------------------------------------------------
state_name(_Event, _From, State) ->
    Reply = ok,
    {reply, Reply, state_name, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Whenever a gen_fsm receives an event sent using
%% gen_fsm:send_all_state_event/2, this function is called to handle
%% the event.
%%
%% @spec handle_event(Event, StateName, State) ->
%%                   {next_state, NextStateName, NextState} |
%%                   {next_state, NextStateName, NextState, Timeout} |
%%                   {stop, Reason, NewState}
%% @end
%%--------------------------------------------------------------------
handle_event(_Event, StateName, State) ->
    {next_state, StateName, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Whenever a gen_fsm receives an event sent using
%% gen_fsm:sync_send_all_state_event/[2,3], this function is called
%% to handle the event.
%%
%% @spec handle_sync_event(Event, From, StateName, State) ->
%%                   {next_state, NextStateName, NextState} |
%%                   {next_state, NextStateName, NextState, Timeout} |
%%                   {reply, Reply, NextStateName, NextState} |
%%                   {reply, Reply, NextStateName, NextState, Timeout} |
%%                   {stop, Reason, NewState} |
%%                   {stop, Reason, Reply, NewState}
%% @end
%%--------------------------------------------------------------------
handle_sync_event(_Event, _From, StateName, State) ->
    Reply = ok,
    {reply, Reply, StateName, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_fsm when it receives any
%% message other than a synchronous or asynchronous event
%% (or a system message).
%%
%% @spec handle_info(Info,StateName,State)->
%%                   {next_state, NextStateName, NextState} |
%%                   {next_state, NextStateName, NextState, Timeout} |
%%                   {stop, Reason, NewState}
%% @end
%%--------------------------------------------------------------------

%% the sensors took too long to communicate
%% maybe the xbee process needs a restart
handle_info({timeout, Timer, sensor}, _SN, #state{sensor_timer=Timer}=S) ->
    lager:info("sensors timed out after in ~p mn",[?SENSOR_TIMEOUT]),
    {stop, sensor_timeout, S};


%% when the FSM times out, it goes to: start
handle_info({timeout, _TimerRef, Msg}, _StateName, #state{buffer=B}=S) ->
    %% lager:info("Timeout: ~w, buffer: ~w~n", [Msg, B]),
    {next_state, start, 
     S#state{buffer= <<>>, length=undefined, timer=undefined}};
%% when we received a bunch of bytes from the serial, 
%% we stream each byte as a series of events 
%% This allows the fsm to deal with a single byte at a time
handle_info({data, Bytes}, StateName, #state{sensor_timer=SensorTimer}=State) ->
    %% First, we clear the sensor timer
    _ = erlang:cancel_timer(SensorTimer),
    % lager:info("xbee received {data, ~w} in state: ~w~n",[Bytes, State]),
    lists:foreach(
      fun(Byte) ->
	      %%io:format("---> ~w~n",[Byte]),
	      %%%gen_fsm:send_event(self(), {data, <<Byte>>}) end,
	      self() ! {escape, <<Byte>>} end,

      binary:bin_to_list(Bytes)),
    {next_state, StateName, State};
%%  if byte is 0x7D, we escape the following one by XORing it with 0x20 
handle_info({escape,<<16#7D>>}, StateName, State) ->
    %% lager:info("xbee received 7D --> escaping~n"),
    {next_state, StateName, State#state{escaping=true}};
handle_info({escape,<<Byte>>}, StateName, #state{escaping=true}=State) ->
    %%gen_fsm:send_event(self(), {data, <<(Byte-16#20)>>}),
    gen_fsm:send_event(self(), {data, <<(Byte bxor 16#20)>>}),
    {next_state, StateName, State#state{escaping=false}};
handle_info({escape,Byte}, StateName, State) ->
    gen_fsm:send_event(self(), {data, Byte}),
    {next_state, StateName, State#state{escaping=false}};
handle_info(_Info, StateName, State) ->
    {next_state, StateName, State}.


%% -spec send_event(pid(),{'data', binary()}) -> 'ok'.
%% send_event(Pid, {data, Byte}) ->
%%    gen_fsm:send_event(self(), {data, Byte}).


%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_fsm when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_fsm terminates with
%% Reason. The return value is ignored.
%%
%% @spec terminate(Reason, StateName, State) -> void()
%% @end
%%--------------------------------------------------------------------
terminate(_Reason, _StateName, #state{serial=Serial}) ->
    Serial ! stop, %% what if this xbee process crashes ?
    ok.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Convert process state when code is changed
%%
%% @spec code_change(OldVsn, StateName, State, Extra) ->
%%                   {ok, StateName, NewState}
%% @end
%%--------------------------------------------------------------------
code_change(_OldVsn, StateName, State, _Extra) ->
    {ok, StateName, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================
check(CheckSum, Bytes) ->
    ByteList = binary:bin_to_list(Bytes),
    <<Sum>> = lists:foldl(fun(X,<<Y>>) -> <<(X+Y)>> end, <<0>>, ByteList),
    %% lager:info("XBEE: CHECKSUM expects ~w, got ~w~n",[CheckSum, (16#FF - Sum)]),
    (16#FF - Sum) == CheckSum.

%% Send the payload to the main module, if the checksums agree
forward(true, Bytes) ->
    %% lager:info("XBEE: CHECKSUM OK, ~w~n",[Bytes]),
    %% extract payload (from byte 14) 
    Payload = binary:part(Bytes, 14, byte_size(Bytes) - 14),
    %% Send payload to main 
    enigma_router_main:sensor_data(Payload);
forward(false, _) ->
    lager:warning("XBEE: DIDNT CHECKSUM!!!~n").


%% @todo turn into a eunit test
%% test_bytes() -> 
%%     Bytes = <<16#10, 16#01, 16#00, 16#13, 16#A2, 16#00, 16#40, 16#0A, 
%%               16#01, 16#27, 16#FF, 16#FE, 16#00, 16#00, 16#54, 16#78, 
%%               16#44, 16#61, 16#74, 16#61, 16#30, 16#41>>,
%%     CheckSum = 16#13,
%%     check(CheckSum, Bytes).

%%%===================================================================
%%% Messages to serial port
%%%===================================================================
% outgoing messages: SerialPort ! Msg
% {send, Bytes} 
% {connect} 
% {disconnect} 
% {open, TTY} 
% {close} 
% {speed, NewInSpeed, NewOutSpeed} 
% {speed, NewSpeed} 
% {parity_odd} 
% {parity_even} 
 % {break} 
% stop
%
% incoming messages
% {data, Bytes}
% {'EXIT', SerialPort, Why} 
