%%%-------------------------------------------------------------------
%%% @author manu
%%% @doc This module implements the modem state machine
%%%  
%%% @end
%%% Created : 30 Jul 2013 by manu <manu@chleb>
%%%-------------------------------------------------------------------
-module(enigma_router_modem).

-behaviour(gen_fsm).
-compile([{parse_transform, lager_transform}]).

%% API
%% -export([start_link/0, sensor_data/1, signal_strength/0]).

%% Tests
-compile(export_all).
%%-export([extract_matches/2, send/2, stop/0]).

%% gen_fsm callbacks
-export([init/1, 
         start/2, signal/2, network/2, gprs/2, connect/2,
         connected/2, open_tcp/2, send_tcp/2, %% close_tcp/2,
         state_name/3, handle_event/3, handle_sync_event/4, 
         handle_info/3, terminate/3, code_change/4]).


-include("enigma_router.hrl").

-define(MAX_RESTART, 10).
-define(MODEM_TIMEOUT, 3000).
-define(DEVICE, "/dev/ttyAMA0").

-record(st, {serial, 
             buffer = <<>>, 
             pattern, 
             timer, 
             data, 
             signal, 
             restart, 
             status :: 'init' | 'waiting'
            }).

%%%===================================================================
%%% API
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Creates a gen_fsm process which calls Module:init/1 to
%% initialize. To ensure a synchronized start-up procedure, this
%% function does not return until Module:init/1 has returned.
%%
%% @spec start_link() -> {ok, Pid} | ignore | {error, Error}
%% @end
%%--------------------------------------------------------------------
start_link() ->%% @todo: FSM do the state progression backward
    
    gen_fsm:start_link({local, ?MODULE}, ?MODULE, [], []).

%% to send sensor data to modem (used by enigma_router_main)
sensor_data(Data) ->
    %% lager:info("Modem received sensor data: ~w",[Data]),
    gen_fsm:send_event(?MODULE, {sensor_data, Data}).

signal_strength() ->
    %% error_logger:info_msg("Signal strength has been requested..."),
    gen_fsm:sync_send_all_state_event(?MODULE, signal_strength).

%% for tests
power1() ->
    gen_fsm:send_all_state_event(?MODULE, power_on).
power2() ->
    gen_fsm:send_all_state_event(?MODULE, power_off).
power3() ->
    gen_fsm:send_all_state_event(?MODULE, power_hh).
uart() ->
    gen_fsm:send_all_state_event(?MODULE, uart).
stop() ->
    gen_fsm:send_all_state_event(?MODULE, stop).

%%%===================================================================
%%% gen_fsm callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Whenever a gen_fsm is started using gen_fsm:start/[3,4] or
%% gen_fsm:start_link/[3,4], this function is called by the new
%% process to initialize.
%%
%% @spec init(Args) -> {ok, StateName, State} |
%%                     {ok, StateName, State, Timeout} |
%%                     ignore |
%%                     {stop, StopReason}
%% @end
%%--------------------------------------------------------------------
init([]) ->
    
%    dbg:tracer(),
%    dbg:p(self(),[m]),
    %% lager:info("GPIO Env: ~p",[application:get_all_env(gpio)]),
    Serial = serial:start([{speed,115200},{open,?DEVICE}]),
    %% lager:info("modem serial ready ~w",[Serial]),
    ok = gpio_init(),
    AtReady = gpio:get(27),
    lager:info("modem gpio initialized, AT ready ? ~p",[AtReady]),
    {ok, start, #st{serial=Serial, pattern="(OK)", restart=0, status=init}, 0}. %% do initialization in 'start' 


%%--------------------------------------------------------------------
%% @private
%% @doc
 
%% There should be one instance of this function for each possible
%% state name. Whenever a gen_fsm receives an event sent using
%% gen_fsm:send_event/2, the instance of this function with the same
%% name as the current state name StateName is called to handle
%% the event. It is also called if a timeout occurs.
%%
%% @spec state_name(Event, State) ->
%%                   {next_state, NextStateName, NextState} |
%%                   {next_state, NextStateName, NextState, Timeout} |
%%                   {stop, Reason, NewState}
%% @end
%%--------------------------------------------------------------------
%% Start state: check to see if modem responds
start({res,_Res}, #st{status=waiting}=S) ->
	lager:info("FSM start: uart ok"),
    {next_state, signal, S#st{restart=0, status=init}, 0};

start(modem_timeout, #st{status=waiting, restart=?MAX_RESTART}=S) -> %% too many restarts
    {stop, modem_too_many_restarts, S};

start(modem_timeout, #st{status=waiting, restart=Restart}=S) -> 
    lager:info("FSM start: restart modem with pattern ~p",[S#st.pattern]),
    power_off(),
    %% Timeout=0: either 'timeout' or any other event will cause "AT\r\n" to be sent
    {next_state, start, S#st{status=init, restart=Restart+1}, 0}; 

start(_Event, #st{serial=Serial, status=init}=S) -> %% likely a 'timeout' or 'sensor_data' event 
    lager:info("FSM start: send AT"),
    Timer = send(Serial, <<"AT\r\n">>),
    {next_state, start, S#st{pattern="(OK)", timer=Timer, status=waiting}};

start(Event, #st{status=waiting}=S)-> 
    lager:info("FSM start: discard ~p",[Event]), 
    {next_state, start, S}.




%% Signal: get signal strength
signal({res,[Strength|_]}, #st{status=waiting}=S) ->
    Sig = bin_to_integer(Strength),
    case Sig of
        99 ->
            lager:info("FSM signal: signal is unknown (99)"), 
            {next_state, start, S#st{status=init}, 0}; %% go back to checking if modem responds
        N when N >= 0 ->
            lager:info("FSM signal: signal strength ok"),
            {next_state, network, S#st{signal=Sig, status=init}, 0};
        _Else ->
            lager:info("FSM signal: signal is negative"), 
            {next_state, start, S#st{status=init}, 0} %% go back to checking if modem responds
    end;

signal(modem_timeout, #st{status=waiting}=S) -> 
    %% {next_state, start, S#st{status=init}, 0}; %% go back to checking if modem responds
    {stop, signal_timeout, S};

signal(_Event, #st{serial=Serial, status=init}=S) -> 
    lager:info("FSM signal: ask signal strength"),
    Timer = send(Serial, <<"AT+CSQ\r\n">>), 
    {next_state, signal, 
     S#st{pattern="\\+CSQ: ([0-9]+),99", timer=Timer, status=waiting}};

signal(Event, #st{status=waiting}=S) -> 
    lager:info("FSM signal: discard ~p",[Event]),
    {next_state, signal, S}.




%% Network: check network registration
network({res,[Status|_]}, #st{status=waiting}=S) ->
%    case bin_to_integer(Status) of
%        1 ->
    lager:info("FSM network: registration: ~p",[Status]),
    {next_state, gprs, S#st{status=init}, 0};
%        _Else ->
%            lager:info("FSM: network not ok, status~p",[Status]),
%            {next_state, signal, State, ?FAIL_DELAY}
%    end;

network(modem_timeout, #st{status=waiting}=S) -> 
    %% {next_state, start, S#st{status=init}, 0}; %% go back to checking if modem responds
    {stop, network_timeout, S};

network(_Event, #st{serial=Serial, status=init}=S) -> 
    lager:info("FSM network: check registration"),
    Timer = send(Serial, <<"AT+CREG?\r\n">>),
    {next_state, network, 
     S#st{pattern="\\+CREG: 1,([0-5])", timer=Timer, status=waiting}};

network(Event, #st{status=waiting}=S) -> 
    lager:info("FSM network: discard ~p",[Event]),
    {next_state, network, S}.




%% GPRS: check gprs registration
gprs({res,_Res}, #st{status=waiting}=S) ->
    lager:info("FSM gprs: registration ok"),
    {next_state, connect, S#st{status=init}, 0};

gprs(modem_timeout, #st{status=waiting}=S) -> 
    %% {next_state, start, S#st{status=init}, 0}; %% go back to checking if modem responds
    {stop, gprs_timeout, S};

gprs(_Event, #st{serial=Serial, status=init}=S) -> 
    lager:info("FSM gprs: checking gprs registration"),
    Timer = send(Serial, <<"AT+AIPDCONT=\"www.vodafone.net.nz\"\r\n">>),
    {next_state, gprs, S#st{pattern="(OK)", timer=Timer, status=waiting}};

gprs(Event, #st{status=waiting}=S) -> 
    lager:info("FSM gprs: discard ~p",[Event]),
    {next_state, gprs, S}.




%% Connect 
connect({res, [<<"ERROR: 302">>]}, #st{status=waiting}=S) ->
    lager:info("FSM connect: modem error: 302"),
    {stop, modem_302, S};

connect({res,Res}, #st{status=waiting}=S) ->
    lager:info("FSM: connect ok, res:~p",[Res]),
    {next_state, connected, S};

connect(modem_timeout, #st{status=waiting}=S) -> 
    %% {next_state, start, S#st{status=init}, 0}; %% go back to checking if modem responds
    {stop, connect_timeout, S};

connect(_Event, #st{serial=Serial, status=init}=S) -> 
    lager:info("FSM connect: try to connect"),
    Timer = send(Serial, <<"AT+AIPA=1\r\n">>),
    {next_state, connect, S#st{pattern="(OK)|(ERROR: 302)", timer=Timer, status=waiting}};

connect(Event, #st{status=waiting}=S) -> 
    lager:info("FSM connect: discard ~p",[Event]),
    {next_state, connect, S}.




%% Connected: in this state, we can process sensor_data events
connected({sensor_data, Data}, S) when is_binary(Data) ->
    lager:info("FSM connected: send TCP data"),
    {next_state, open_tcp, S#st{data=Data, status=init}, 0};

connected(Event, S) -> 
    lager:info("FSM connected: discard ~p",[Event]),
    {next_state, connected, S}.




%% Open TCP socket
open_tcp({res,_Res}, #st{status=waiting}=S) ->
    lager:info("FSM open_tcp: tcp opened ok"),
    {next_state, send_tcp, S#st{status=init}, 0};

open_tcp(modem_timeout, #st{status=waiting}=S) -> 
    {next_state, connect, S#st{status=init}, 0}; %% Go back and try establishing a connection

open_tcp(_Event, #st{serial=Serial, status=init}=S) ->
    lager:info("FSM open_tcp: opening tcp socket"),
    Timer = send(Serial, <<"AT+AIPO=1,,\"50.116.12.207\",5555,0\r\n">>),
    {next_state, open_tcp, S#st{pattern="(OK)", timer=Timer, status=waiting}};
open_tcp(Event, #st{status=waiting}=S) -> 
    lager:info("FSM open_tcp: discard ~p",[Event]),
    {next_state, open_tcp, S}.



%% Send TCP data
send_tcp({res,_Res}, #st{status=waiting}=S) ->
    lager:info("FSM: send_tcp ok"),
    {next_state, close_tcp, S#st{status=init}, 0};

send_tcp(modem_timeout, #st{status=waiting}=S) -> 
    {next_state, connect, S#st{status=init}, 0}; 

send_tcp(_Event, #st{serial=Serial, data=Data, status=init}=S) ->
    lager:info("FSM send_tcp: sending tcp data ~p",[Data]),
    CommandStr = ["AT+AIPW=1,\"", to_hex(Data), "\"\r\n"],
    Timer = send(Serial, list_to_binary(CommandStr)),
    {next_state, send_tcp, S#st{pattern="(OK)", timer=Timer, status=waiting}};

send_tcp(Event, #st{status=waiting}=S) -> 
    lager:info("FSM send_tcp: discard ~p",[Event]),
    {next_state, send_tcp, S}.



%% Close TCP socket
close_tcp({res,_Res}, #st{status=waiting}=S) ->
    lager:info("FSM close_tcp: socket closed ok"),
    {next_state, connect, S#st{status=init}, 0}; 
close_tcp(modem_timeout, #st{status=waiting}=S) -> 
    {next_state, connect, S#st{status=init}, 0}; 

close_tcp(_Event, #st{serial=Serial, status=init}=S) ->
    lager:info("FSM close_tcp: closing tcp socket"),
    Timer = send(Serial, <<"AT+AIPC=1\r\n">>),
    {next_state, close_tcp, S#st{pattern="(OK)", timer=Timer, status=waiting}};

close_tcp(Event, #st{status=waiting}=S) -> 
    lager:info("FSM close_tcp: discard ~p",[Event]),
    {next_state, close_tcp, S}.



%%--------------------------------------------------------------------
%% @private
%% @doc
%% There should be one instance of this function for each possible
%% state name. Whenever a gen_fsm receives an event sent using
%% gen_fsm:sync_send_event/[2,3], the instance of this function with
%% the same name as the current state name StateName is called to
%% handle the event.
%%
%% @spec state_name(Event, From, State) ->
%%                   {next_state, NextStateName, NextState} |
%%                   {next_state, NextStateName, NextState, Timeout} |
%%                   {reply, Reply, NextStateName, NextState} |
%%                   {reply, Reply, NextStateName, NextState, Timeout} |
%%                   {stop, Reason, NewState} |
%%                   {stop, Reason, Reply, NewState}
%% @end
%%--------------------------------------------------------------------
state_name(_Event, _From, State) ->
    Reply = ok,
    {reply, Reply, state_name, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Whenever a gen_fsm receives an event sent using
%% gen_fsm:send_all_state_event/2, this function is called to handle
%% the event.
%%
%% @spec handle_event(Event, StateName, State) ->
%%                   {next_state, NextStateName, NextState} |
%%                   {next_state, NextStateName, NextState, Timeout} |
%%                   {stop, Reason, NewState}
%% @end
%%--------------------------------------------------------------------
%% handle_event({sensor_data, _Data}, StateName, State) when StateName == signal; StateName == gprs; StateName == network->
%%     lager:info("FSM: receive {sensor_data,_} in state ~w",[StateName]),
%%     {next_state, StateName, State, 0};
handle_event(stop, _StateName, State) ->
    {stop, normal, State};
handle_event(_Event, StateName, State) ->
    {next_state, StateName, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Whenever a gen_fsm receives an event sent using
%% gen_fsm:sync_send_all_state_event/[2,3], this function is called
%% to handle the event.
%%
%% @spec handle_sync_event(Event, From, StateName, State) ->
%%                   {next_state, NextStateName, NextState} |
%%                   {next_state, NextStateName, NextState, Timeout} |
%%                   {reply, Reply, NextStateName, NextState} |
%%                   {reply, Reply, NextStateName, NextState, Timeout} |
%%                   {stop, Reason, NewState} |
%%                   {stop, Reason, Reply, NewState}
%% @end
%%--------------------------------------------------------------------
%% handle_sync_event(serial, _From, StateName, #st{serial=Serial}=State) ->
%%     error_logger:error_msg("serial: current state --> ~w~n",[StateName]),
%%     {reply, Serial, StateName, State};
handle_sync_event(signal_strength, _From, StateName, #st{signal=undefined}=S) ->
    %%lager:info("FSM: received signal_strength in state ~w with signal undefined", [StateName]),
    {reply, undefined, StateName, S};
handle_sync_event(signal_strength, _From, StateName, #st{signal=Sig}=S) ->
    %%lager:info("FSM: received signal_strength in state ~w with signal = ~w", [StateName, Sig]),
    {reply, Sig, StateName, S};
handle_sync_event(Event, _From, StateName, State) ->
    lager:info("FSM: received ~w in state ~w with state ~w", [Event, StateName, State]),
    Reply = ok,
    {reply, Reply, StateName, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_fsm when it receives any
%% message other than a synchronous or asynchronous event
%% (or a system message).
%%
%% @spec handle_info(Info,StateName,State)->
%%                   {next_state, NextStateName, NextState} |
%%                   {next_state, NextStateName, NextState, Timeout} |
%%                   {stop, Reason, NewState}
%% @end
%%--------------------------------------------------------------------

%% the modem took too long to respond
handle_info({timeout, Timer, modem}, SN, #st{timer=Timer}=S) ->
    lager:info("modem timed out in ~p with ~p",[SN,S#st.pattern]),
    gen_fsm:send_event(?MODULE, modem_timeout),
    {next_state, SN, S};

%% Empty pattern means we discard incoming data (not awaiting an answer)
%% handle_info({data, Bytes}, start, #st{pattern=undefined}=S) ->
%%     lager:info("handle_info discard ~p, but go from start to signal anyway",[Bytes]),
%%     {next_state, signal, ?zero(S)};

handle_info({data, Bytes}, SN, #st{pattern=undefined}=S) ->
    %% lager:info("handle_info discard ~p in state ~p with ~p",[Bytes, SN, S#st.pattern]),
    {next_state, SN, S};

%% Set the timer, when first bytes arrive...
%% handle_info({data, Bytes}, SN, #st{buffer= <<>>,timer=undefined}=S) ->
%%     lager:info("handle_info starts timer"),
%%     {next_state, SN, S#st{buffer=Bytes, timer=Timer}};

handle_info({data, Bytes}, SN, #st{buffer=B,pattern=P,timer=T}=S) ->
    %% lager:info("handle_info - bytes: ~p in state: ~p with ~p",[Bytes, SN, P]),
    B2 = <<B/binary, Bytes/binary >>,
            %% search the buffer B2 for pattern P
            case re:run(B2, P, [{capture, all_but_first}]) of
                nomatch -> 
                    %% lager:info("no match ~s ?= ~s",[B2, P]),
                    {next_state, SN, S#st{buffer=B2}};
                % {match,[{10,3},{14,2},{17,2},{20,2}]}
                %% {match, [{-1,0}|_]=M} ->
                %%     lager:info("Modem: wrong markers: binary ~s, pattern ~s, markers ~wn", [B2,P,M]),
                %%     {next_state, SN, S#st{buffer=B2}};
                {match, Markers} -> 
                    _ = erlang:cancel_timer(T), %% cancel timeout
                    Res = extract_matches(Markers, B2),
                    gen_fsm:send_event(?MODULE, {res, Res}),
                    lager:info("Modem: matches ~p", [Res]),
                    {next_state, SN, S#st{buffer= <<>>, pattern=undefined}}
            end;

handle_info(Info, SN, State) ->
    %% lager:info("handle_info - unknown evt: ~p - in state: ~p",[Info, SN]),
    {next_state, SN, State}.
%%
%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_fsm when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_fsm terminates with
%% Reason. The return value is ignored.
%%
%% @spec terminate(Reason, StateName, State) -> void()
%% @end
%%--------------------------------------------------------------------
terminate(_Reason, _StateName, #st{serial=_Serial}) ->
    %% @todo this should check whether the modem is still connected beforehand
    %% disconnect_network(Serial), 
    power_off(),
    ok.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Convert process state when code is changed
%%
%% @spec code_change(OldVsn, StateName, State, Extra) ->
%%                   {ok, StateName, NewState}
%% @end
%%--------------------------------------------------------------------
code_change(_OldVsn, StateName, State, _Extra) ->
    {ok, StateName, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================

extract_matches([], _Bin) -> [];
extract_matches([{Start, _Length}|Markers], Bin) when Start < 0 ->
    extract_matches(Markers, Bin);
extract_matches([{Start, Length}|Markers], Bin) ->
    [binary:part(Bin, Start, Length)|extract_matches(Markers, Bin)].



-spec send(pid(), binary()) -> reference().
send(Serial, Command) ->
    %% lager:info("Send to modem ~w~s",[Command]),
    Serial ! {send, Command}, 
    erlang:start_timer(?MODEM_TIMEOUT, self(), modem).

bin_to_integer(Bin) ->
    list_to_integer(binary_to_list(Bin)).

%% convert decimal bytes to hexa bytes
%% to_hex(<<0,0,0,1,6,1,0,0,0,4,129>>) --> <<"0000000106010000000481">>
to_hex(Bin) ->
    list_to_binary([ byte_to_hex(Byte) || <<Byte>> <= Bin ]).
byte_to_hex(N) when N < 16 ->
    ["0", integer_to_list(N,16)];
byte_to_hex(N) -> 
    integer_to_list(N,16).

%% convert hexa bytes to decimal bytes                 
%% to_dec(<<"0000000106010000000481">>) --> <<0,0,0,1,6,1,0,0,0,4,129>>
to_dec(Bin) ->
    list_to_binary([list_to_integer([X,Y], 16) || <<X,Y>> <= Bin]).

%% compile_patterns(Patterns) ->
%%    lists:map(fun(P) -> {_, P2} = re:compile(P), P2 end, Patterns).

%%%===================================================================
%%% Modem commands
%%%===================================================================
gpio_init() ->
    A = gpio:release(?ONKEY),
    B = gpio:init(?ONKEY),
    C = gpio:set_direction(?ONKEY, low),
    D = gpio:release(?DSR_CTS),
    E = gpio:init(?DSR_CTS),
    F = gpio:set_direction(?DSR_CTS, high),

    error_logger:info_msg("release onkey:~p~ninit onkey:~p~n"
			  "setdir onkey:~p~nrelease dsr:~p~n"
			  "init dsr:~p~nsetdir:~p~n",[A,B,C,D,E,F]).



%% simple approach to restarting modem when not responsive
reset1() ->
     power_on().

%% other approach to restarting modem when not responsive
%% check state of ONKEY and DSR_CTS pins first...
reset2() ->
    {ok, OnKey} = gpio:get(?ONKEY), %% {ok,uint1()} | {error,Reason::posix()}.
    {ok, Dsr} = gpio:get(?DSR_CTS),
    %% lager:info("ONKEY: ~p, DSR: ~p",[OnKey,Dsr]),
    case {OnKey, Dsr} of
        %% {0, 0} -> do_nothing;
        {0, 1} -> power_on(); 
        {_, _} -> power_off(), power_on()
    end.

            

power_on() ->
    lager:info("Modem: Powering Up - START"),
    %% lager:info("Modem: ONKEY=Low, DSR/CTS=High"),
    timer:sleep(500), %% t1 > 200ms
    ok = gpio:set(?ONKEY),
    %% lager:info("Modem: ONKEY=High, DSR/CTS=High"),
    timer:sleep(2500), %% t2 > 2000ms
    ok = gpio:clr(?ONKEY),
    %% lager:info("Modem: ONKEY=LOW, DSR/CTS=High"),
    timer:sleep(3000), %% t3 > 2500ms
    ok = gpio:set(?DSR_CTS),
    %% lager:info("Modem: ONKEY=Low, DSR/CTS=Low"),
    timer:sleep(200),  %% t4 > 100ms
    lager:info("Modem: Powering Up - END - WAIT 15sec"),
    timer:sleep(15000). %% 15 sec


power_off() ->
    lager:info("Modem: Powering Down - START"),
    %% lager:info("Modem: ONKEY=Low, DSR/CTS=High"),
    ok = gpio:clr(?ONKEY),
    ok = gpio:clr(?DSR_CTS),
    timer:sleep(500), %% t1 > 200ms
    %% lager:info("Modem: ONKEY=High, DSR/CTS=High"),
    ok = gpio:set(?ONKEY),
    timer:sleep(2500), %% t2 > 2000ms
    %% lager:info("Modem: ONKEY=LOW, DSR/CTS=High - WAIT 6.5sec"),
    ok = gpio:clr(?ONKEY),
    timer:sleep(6500), %% t3 > 6000ms
    lager:info("Modem: Powering Down - END - WAIT 15sec"),
    timer:sleep(15000). %% 15 sec

power_hh() ->
    %% lager:info("Modem: Power3 - START"),
    ok = gpio:clr(?ONKEY),
    ok = gpio:clr(?DSR_CTS),
    timer:sleep(500), %% t1 > 200ms
    %% lager:info("Modem: ONKEY=High, DSR/CTS=High"),
    ok = gpio:set(?ONKEY),
    timer:sleep(2500), %% t2 > 2000ms
    lager:info("Modem: Power3 - END").



%% initialize_modem(Serial) ->
%%     %% Turn off the Modem ECHO (Hit and hope from now on?)
%%     %lager:info("Modem: Turn off the Modem ECHO"),
%%     %echo_off(Serial),
%%    
%%     %% Check if GSM/GPRS module is ready to work (has network service)
%%     lager:info("Modem: check gprs signal strength"),
%%     ok = check_signal_strength(get_signal_strength(Serial), 5),
%%     %% Show status of network registration
%%     lager:info("Modem: Show status of network registration"),
%%     show_network_registration(Serial),
%%     %% Check status of network registration
%%     lager:info("Modem: Check status of network registration"),
%%     check_network_registration(Serial),
%%     %%  Set the GPRS service parameter (apn='www.vodafone.net.nz')
%%     lager:info("Modem: Set the GPRS service parameter"),
%%     set_gprs_conn(Serial),
%%     %% Attach to the GPRS network, if not connected already
%%     lager:info("Modem: connect to the network"),
%%     connect_network(Serial).
%% 
%% 
%% tcp_send(Serial, Data) ->
%%     open_tcp(Serial),
%%     send_tcp_data(Serial,Data),
%%     close_tcp(Serial).

%%%-----------------------------------------------------------------------------
%% NOTE: the following commands can cause the FSM to restart (timeout)
%% So they should not be used in the starting states !!!

check_uart(Serial) ->
    send(Serial, <<"AT\r\n">>).
%% 
%% 
%% -spec get_signal_strength(pid()) -> pos_integer().
%% get_signal_strength(Serial) ->
%%     {ok, [Strength|_]} = send_and_ack(Serial, <<"AT+CSQ\r\n">>, "CSQ: ([0-9]+),99[\r\n]*OK"),
%%     bin_to_integer(Strength).
%% 
%% -spec check_signal_strength(pos_integer(), pos_integer()) -> 'ok' | 'weak'.
%% check_signal_strength(Signal, Min) when Signal > Min -> ok;
%% check_signal_strength(_, _) -> weak.
%% 
%% 
%% show_network_registration(Serial) ->
%%     send_and_ack(Serial, <<"AT+CREG=1\r\n">>).
%% 
%% set_gprs_conn(Serial) ->
%%     send_and_ack(Serial, <<"AT+AIPDCONT=\"www.vodafone.net.nz\"\r\n">>).
%% 
%% check_network_registration(Serial) ->
%%     {ok, [Status|_]} = send_and_ack(Serial, <<"AT+CREG?\r\n">>, "CREG: 1,([0-5])[\r\n]*OK"),
%%     bin_to_integer(Status).
%% 
%% connect_network(Serial) ->
%%     send_and_ack(Serial, <<"AT+AIPA=1\r\n">>).
%% 
%% disconnect_network(Serial) ->
%%     send_and_ack(Serial, <<"AT+AIPA=0\r\n">>).
%%     
%% check_network(Serial) ->
%%     send_and_ack(Serial, <<"AT+AIPA?\r\n">>, "(AIPA:1)").
%% 
%% open_tcp(Serial) ->
%%     send_and_ack(Serial, <<"AT+AIPO=1,,\"50.116.12.207\",5555,0\r\n">>).
%% 
%% send_tcp_data(Serial,Data) ->
%%     CommandStr = ["AT+AIPW=1,\"", to_hex(Data), "\"\r\n"],
%%     send_and_ack(Serial, list_to_binary(CommandStr)).
%% 
%% close_tcp(Serial) ->
%%     send_and_ack(Serial, <<"AT+AIPC=1\r\n">>).
%% 
%% echo_on(Serial) ->
%%     send_and_ack(Serial, <<"ATE1\r\n">>).
%% 
%% echo_off(Serial) ->
%%     send_and_ack(Serial, <<"ATE0\r\n">>).
%% 

%%%===================================================================
%%% Messages to serial port
%%%===================================================================
% outgoing messages: SerialPort ! Msg
% {send, Bytes} 
% {connect} 
 % {disconnect} 
% {open, TTY} 
% {close} 
% {speed, NewInSpeed, NewOutSpeed} 
% {speed, NewSpeed} 
% {parity_odd} 
% {parity_even} 
% {break} 
% stop
%
% incoming messages
% {data, Bytes}
% {'EXIT', SerialPort, Why} 


%%%===================================================================
%%% Gpio module info
%%%===================================================================
%% to read more about the gpio module API: https://github.com/Feuerlabs/gpio

%% test(Serial) ->
%%       lager:info("uart ~p",[check_uart(Serial)]),
%%       lager:info("sig strength ~p",[get_signal_strength(Serial)]),
%%       lager:info("checksig ~p",[check_signal_strength(
%%                        get_signal_strength(Serial), 5)]),
%%       lager:info("show_net ~p",[show_network_registration(Serial)]),
%%       lager:info("check_net ~p",[check_network_registration(Serial)]),
%%       lager:info("set_gprs ~p",[set_gprs_conn(Serial)]),
%%       lager:info("conn_net ~p",[connect_network(Serial)]),
%%       lager:info("open_tcp ~p",[open_tcp(Serial)]),
%%       lager:info("send_tcp ~p",[send_tcp_data(Serial,<<"Hello!">>)]),
%%       lager:info("clo_tcp ~p",[close_tcp(Serial)]),
%%       lager:info("dis_conn ~p",[disconnect_network(Serial)]),
%%       lager:info("echon ~p",[echo_on(Serial)]),
%%       lager:info("echoff ~p", [echo_off(Serial)]).

