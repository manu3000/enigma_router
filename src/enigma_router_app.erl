-module(enigma_router_app).

-behaviour(application).


%% Application callbacks
-export([start/2, stop/1]).

-include("enigma_router.hrl").

%% ===================================================================
%% Application callbacks
%% ===================================================================

start(_StartType, _StartArgs) ->
    %% HTTP Server
    Dispatch = cowboy_router:compile([
		{'_', [
			{"/", cowboy_static, [
				{directory, {priv_dir, enigma_router, []}},
				{file, <<"index.html">>}, %% dashboard
				{mimetypes, [{<<".html">>, [<<"text/html">>]}]}
			]},
      %% {"/alert_config", alert_config_handler, []},

      %% Disable dashboard for now 
      %% (it needs to be updated to use a new config format in enigma_router_config)
      {"/websocket", ws_handler, []},

			{"/static/[...]", cowboy_static, [
				{directory, {priv_dir, enigma_router, [<<"static">>]}},
        {mimetypes, {fun mimetypes:path_to_mimes/2, default}}
			]}
		]}]),
	  {ok, _} = cowboy:start_http(http, 100, [{port, 8888}], 
                                [{env, [{dispatch, Dispatch}]}]),


    %% GPIO init
    ok = gpio_init(),
    %% Enigma supervisor
    enigma_router_sup:start_link().

stop(_State) ->
    ok.






gpio_init() ->
    %% GPIO
%%    ok = gpio:release(?POWER_ACT),
%%    ok = gpio:init_direct(?POWER_ACT),
%%    ok = gpio:set_direction(?POWER_ACT, high),
    ok = gpio:init(?AT_READY),
    ok = gpio:input(?AT_READY),
    ok = gpio:init(?POWER_ACT),
    ok = gpio:output(?POWER_ACT),
    ok = gpio:init(?POWER_ERR),
    ok = gpio:input(?POWER_ERR),
    ok = gpio:init(?POWER_ON),
    ok = gpio:input(?POWER_ON),
%% GAVIN EDIT 05-08-2014
    ok = gpio:init_direct(?ONKEY),
    ok = gpio:input(?ONKEY),
    ok = gpio:set_direction(?ONKEY, low),
    ok = gpio:init_direct(?DSR_CTS),
    ok = gpio:set_direction(?DSR_CTS, high),
    ok = gpio:input(?DSR_CTS).
