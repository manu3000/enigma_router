%% @doc This module opens a TCP socket and send sensor data
%% It also routinely checks that a TCP connection can be established
%% If opening the socket fails, tells enigma_router_main to use modem 
%% If it succeeds, tells it to use tcp 
%% end

-module(enigma_router_tcp).
-behaviour(gen_server).
-compile([{parse_transform, lager_transform}]).

-export([start_link/0, 
         stop/0, 
         sensor_data/1
        ]).

-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         code_change/3, terminate/2]).


-include_lib("eunit/include/eunit.hrl").

-record(state, {%% socket  :: port(), % the socket
                host :: string(),
                port :: string(),
                tref :: reference()
               }).

-define(SLEEP, timer:minutes(15)).
-define(TCP_OPTIONS, [binary, {packet, 0}, {active, false}]).

%%------------------------
%% API
%%------------------------

%% @doc Start TCP client
-spec start_link() -> {ok, pid()}.
start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

%% @doc Stop TCP client
-spec stop() -> stopped.
stop() ->
    gen_server:call(?MODULE, stop).

%% %% @doc Send an alert for online processing
%% -spec alert(sdata() | iolist()) -> ok.
%% alert(Msg) ->
%%     gen_server:cast(?MODULE, {alert, Msg}).
%% 

%% @doc Send sensor data to the TCP server 
%% to send sensor data to tcp client (used by enigma_router_main)
-spec sensor_data(binary()) -> ok.
sensor_data(Data) -> 
    gen_server:cast(?MODULE, {sensor_data, Data}).

%% @doc Try connecting to online app
%% -spec connect() -> ok.
%% connect() ->
%%     gen_server:cast(?MODULE, connect).

%% %%%% @doc Get online config version
%% %% -spec get_config(pos_integer()) -> ok.
%% %% get_config(LocalVersion) ->
%% %%     gen_server:cast(?MODULE, {config, LocalVersion}).

%% %% @doc send stored data online
%% -spec sync_data(sdata(), pos_integer() | atom()) -> ok.
%% sync_data(SData, Key) ->
%%     gen_server:cast(?MODULE, {sync_data, SData, Key}).

%%------------------------
%% Gen Server Callbacks
%%------------------------
init([]) ->
    lager:info("init tcp~n"),

    %% get ip:port from application's config file
    {ok, {Host, Port}} = application:get_env(server),

    %% start a timer that regularly try to connect
    Ref = erlang:start_timer(0, self(), connect),
    {ok, #state{host=Host, port=Port, tref=Ref}}.


%%--------------------------------------------------------------------
%% Gen Server Calls
%%--------------------------------------------------------------------
handle_call(stop, _From, State) ->
    {stop, normal, stopped, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc Incoming messages from router_alert
%% @end
%%--------------------------------------------------------------------
%% -type cast_msg() :: #sdata{} | connect. 
%%-spec handle_cast(cast_msg(), #state{}) ->
%%                         {noreply, #state{}}. 
%%|                         {noreply, #state{}, pos_integer()}.

%%%% @doc Asking for the router config
%% handle_cast({config, _LocalVersion}, State=#state{connected=false}) ->
%%     {noreply, State};
%% handle_cast({config, LocalVersion}, State=#state{socket=Socket}) ->
%%     Msg = term_to_binary({config, LocalVersion}),
%%     lager:info("Asking for config > ~w~n",[LocalVersion]),
%%     ok = send(Socket, Msg),
%%     %% sent data to server, we expect a response within 30s before timing out 
%%     {noreply, State, ?TIMEOUT};



%% handle_cast(get_config, State=#state{socket=undefined}) ->
%%     {noreply, State};
%% handle_cast(get_config, State=#state{socket=Socket}) ->
%%     %% @doc Ask server for an up to date config, if there is one. (Asynchronous) 
%%     LocalVersion = router_store:config_version(),
%%     Msg = term_to_binary({config, LocalVersion}),
%%     lager:info("Asking for config > ~w~n",[LocalVersion]),
%%     ok = send(Socket, Msg),
%%     {noreply, State};

%% @doc sending sensor data to server
handle_cast({sensor_data, Data}, State=#state{host=Host, port=Port}) ->
    lager:info("Sending data ~w~n",[Data]),
    %% send data over TCP
    send(Host, Port, Data),
    {noreply, State};

%% %% Message used by router_store to upload locally saved sensor data
%% handle_cast({sync_data, SData, _Key}, State=#state{socket=undefined}) ->
%%     %% we can't send this online, so we send it back to router_store
%%     router_store:write_data(SData),
%%     {noreply, State};
%% handle_cast({sync_data, SData, Key}, State=#state{socket=Socket}) ->
%%     Msg = term_to_binary({sdata, SData}),
%%     %lager:info("Syncing: ~p~n", [SData]),
%%     %% send data over TCP
%%     ok = send(Socket, Msg),
%%     %% we ask router_store for the next datum
%%     router_store:sync_data(Key),
%%     {noreply, State};

handle_cast(Msg, State) ->
    lager:info("Got msg: ~w~nstate: ~w~n",[Msg, State]),
    {noreply, State}. 

%%--------------------------------------------------------------------
%% @private
%% @doc All TCP socket messages are processed by handle_info/2
%% @end
%%--------------------------------------------------------------------
-type tcp_msg() :: {tcp, port(), binary()} 
                 | {tcp_closed, port()}
                 | {tcp_error, port(), any()}.

-spec handle_info(tcp_msg() | {timeout, reference(), connect}, #state{}) -> 
                         {noreply, #state{}}.


handle_info({timeout, Ref, connect}, 
            #state{host=Host, port=Port, tref=Ref}=State) ->
    %% try connecting to server
    send(Host, Port, <<"ping">>),
    %% set a new timer to try again later
    NewRef = erlang:start_timer(?SLEEP, self(), connect),
    {noreply, State#state{tref=NewRef}};

handle_info({tcp, _Port, Bin}, State) ->
    %% gen_tcp:setopts(Socket, [{active, once}]),
    %% receive the latest config and send it to gen_event
    %% Msg = binary_to_term(Bin), %% this will blow up when Bin is not an Erlang term
    lager:info("Online server responded: ~w~n", [Bin]), 
%%    case Msg of
%%         no_config -> 
%%             lager:info("Server says: no config change !"), 
%%             ok;
%%         got_alert -> 
%%             lager:info("Server says: received alert !"), 
%%             ok;
%%         got_data  -> 
%%             lager:info("Server says: received data !"), 
%%             ok;
%%         {config, Config} -> 
%%             lager:info("Server says: config update !"), 
%%             router_alert:config(Config);
%%        Other -> 
%%            error_logger:error_msg("Received tcp message: ~w~n", [Other]) 
%%    end,
    {noreply, State};

handle_info({tcp_closed, _Socket}, State) ->
    %% the link is down, we try reconnecting
    error_logger:error_msg("The socket is closed~n"),
    %% router_timer:timer_on(), maybe replace with erlang timer
    %% router_tcp:connect(), 
    {noreply, State}; 


handle_info({tcp_error, _Socket, Reason}, State) ->
    %% gen_tcp:setopts(Socket, [{active, once}]),
    error_logger:error_msg("TCP error: ~p~n", [Reason]),
    {noreply, State};
    % {stop, Reason, State};  % @todo do we want to crash here ?


%% handle_info(timeout, State = #state{socket=undefined}) ->
%%     {noreply, State};
%% handle_info(timeout, State) ->
%%     %% this likely means that the online app is not responding
%%     %% @todo send an alert 
%%     error_logger:error_msg("Timeout: online app has failed to respond~n"),
%%     {noreply, State};

handle_info(_Info, State) -> 
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Convert process state when code is changed
%%
%% @spec code_change(OldVsn, State, Extra) -> {ok, NewState}
%% @end
%%--------------------------------------------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_server terminates
%% with Reason. The return value is ignored.
%%
%% @spec terminate(Reason, State) -> void()
%% @end
%%--------------------------------------------------------------------
terminate(normal, _State) ->
    ok;
terminate(Reason, _State) ->
    error_logger:error_msg("terminate reason: ~p~n", [Reason]).


%%%===================================================================
%%% Internal functions
%%%===================================================================
%% -spec send(Host::string(), Port::string(), Data::binary()) -> 'ok'.
send(undefined, undefined, _Data) -> ok;
send(Host, Port, Data) ->
    case gen_tcp:connect(Host, Port,  ?TCP_OPTIONS, 5000) of
        %% if the link is down
        {error, Reason} ->
            error_logger:error_msg("No TCP: ~p~n",[Reason]),
            enigma_router_main:use_modem();
        %% we got network
        {ok, Socket} -> 
            lager:info("TCP connected: ~w, data: ~p~n", [Socket, Data]),
            ok = gen_tcp:send(Socket, Data),
            gen_tcp:close(Socket),
            enigma_router_main:use_tcp()
    end.

%% send(Socket, Bin) ->
%%     ok = gen_tcp:send(Socket, Bin).
%%     %gen_tcp: setopts(Socket, [{active, once}]).


