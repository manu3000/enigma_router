%% GPIO pins
-define(AT_READY, 27).
-define(POWER_ON, 23).
-define(POWER_ERR, 25).
-define(POWER_ACT, 24).
-define(FAN_ACT, 22).
-define(ONKEY, 7).
-define(DSR_CTS, 8).

-type config() :: [{atom(), number()}].

-type readings() :: [{SensorCode :: integer(), 
                      SensorNum :: integer(),  
                      DT_Code :: integer(), 
                      Value :: float()}].
