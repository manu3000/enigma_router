%%%-------------------------------------------------------------------
%%% @author Emmanuel Delaborde <Emmanuel@ou022055.otago.ac.nz>
%%% @copyright (C) 2014, Emmanuel Delaborde
%%% @doc
%%% This module is a server which monitors sensor data
%%% (proxied via enigma_router_main) and raise alerts if needs be.
%%% The server state keeps track of previous sensor readings.
%%% The alerts settings are obtained from enigma_router_config.
%%% @end
%%% Created : 27 Jun 2014 by Emmanuel Delaborde <Emmanuel@ou022055.otago.ac.nz>
%%%-------------------------------------------------------------------

%% TODO: defined a type for the gen_server state to use in -spec directive
%% test helper functions with eunit

-module(enigma_router_alert).

-behaviour(gen_server).

-include("enigma_router.hrl").

%% API
-export([avg_fan_temp/0, avg_pump_temp/0,
	 sensor_data/1, start_link/0]).

%% gen_server callbacks
-export([code_change/3, handle_call/3, handle_cast/2,
	 handle_info/2, init/1, terminate/2]).

-define(SERVER, ?MODULE).

-include_lib("eunit/include/eunit.hrl").


-type frost_status() :: off | starting | frost | stopping.
-type both_status() :: {frost_status(), frost_status()}.            
-type time() :: {integer(), integer(), integer()}.
-type pump_settings() :: {float(), float(), integer(), integer()}.
-type fan_settings() :: {float(), float(), integer(), integer(), float()}.
-type action() :: 'pump_off_fan_off'
                | 'pump_on_fan_off' 
                | 'pump_off_fan_on'
                | 'no_action'. 

-record(st, {
    status::both_status(), 
    since :: {time(), time()}, %% since for pump and for fan 
    action :: action(), 
    avg_pump_temp :: float(), 
    pump_temps :: [float()],
	avg_fan_temp :: float(), 
    fan_temps :: [float()]
}).

%%%===================================================================
%%% API
%%%===================================================================

-spec sensor_data(readings()) -> ok.

sensor_data(Data) ->
    %%lager:info("main received ~s~n",[Data]),
    gen_server:cast(?SERVER, {sensor_data, Data}).

-spec avg_pump_temp() -> float().

avg_pump_temp() ->
    gen_server:call(?SERVER, avg_pump_temp).

-spec avg_fan_temp() -> float().

avg_fan_temp() ->
    gen_server:call(?SERVER, avg_fan_temp).

%%--------------------------------------------------------------------
%% @doc
%% Starts the server
%%
%% @spec start_link() -> {ok, Pid} | ignore | {error, Error}
%% @end
%%--------------------------------------------------------------------
start_link() ->
    gen_server:start_link({local, ?SERVER}, ?MODULE, [],
			  []).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Initializes the server
%%
%% @spec init(Args) -> {ok, State} |
%%                     {ok, State, Timeout} |
%%                     ignore |
%%                     {stop, Reason}
%% @end
%%--------------------------------------------------------------------
init([]) ->
    State = #st{
        status = {off, off}, 
        since = {os:timestamp(), os:timestamp()},
		action = pump_off_fan_off, 
        avg_pump_temp = 30.0,
		pump_temps = [], 
        avg_fan_temp = 30.0, 
        fan_temps = []
    },
    {ok, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling call messages
%%
%% @spec handle_call(Request, From, State) ->
%%                                   {reply, Reply, State} |
%%                                   {reply, Reply, State, Timeout} |
%%                                   {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, Reply, State} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_call(avg_pump_temp, _From,
	    #st{avg_pump_temp = AvgPumpTemp} = State) ->
    {reply, AvgPumpTemp, State};
handle_call(avg_fan_temp, _From,
	    #st{avg_fan_temp = AvgFanTemp} = State) ->
    {reply, AvgFanTemp, State};
handle_call(_Request, _From, State) ->
    Reply = ok, {reply, Reply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling cast messages
%%
%% @spec handle_cast(Msg, State) -> {noreply, State} |
%%                                  {noreply, State, Timeout} |
%%                                  {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_cast({sensor_data, Data},
	    #st{status = OldStatuses, since = OldSince,
		pump_temps = OldPumpTemps,
        fan_temps = OldFanTemps}) ->
    %% keep the 9 newest pump temperatures
    NewPumpTemps = lists:sublist(get_pump_temps(Data) ++
				   OldPumpTemps,
				 9),
    %% keep the 9 newest fan temperatures
    NewFanTemps = lists:sublist(get_fan_temps(Data) ++
				   OldFanTemps,
				 9),
    AvgPumpTemp = average_nine(NewPumpTemps),
    AvgFanTemp = average_nine(NewFanTemps),
    error_logger:info_msg("New AvgTemp ~w~n", [AvgPumpTemp]),
    {NewStatuses, NewSince, Action} = new_alert_status(OldStatuses,
					     OldSince, AvgPumpTemp, AvgFanTemp),
    %% action the pump/fan if needed
    execute(Action),
    NewState = #st{status = NewStatuses, 
            since = NewSince,
		    avg_pump_temp = AvgPumpTemp,
		    pump_temps = NewPumpTemps,
            action = Action, 
            avg_fan_temp = AvgFanTemp,
		    fan_temps = NewFanTemps},
    error_logger:info_msg("new state ~p~n", [NewState]),
    {noreply, NewState};
handle_cast(_Msg, State) -> {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling all non call/cast messages
%%
%% @spec handle_info(Info, State) -> {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_info(_Info, State) -> {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_server terminates
%% with Reason. The return value is ignored.
%%
%% @spec terminate(Reason, State) -> void()
%% @end
%%--------------------------------------------------------------------
terminate(_Reason, _State) -> ok.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Convert process state when code is changed
%%
%% @spec code_change(OldVsn, State, Extra) -> {ok, NewState}
%% @end
%%--------------------------------------------------------------------
code_change(_OldVsn, State, _Extra) -> {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================
%-spec new_alert_status(#st{}) -> #st{}.
new_alert_status({OldPumpStatus, OldFanStatus}, {OldPumpSince, OldFanSince}, AvgGrdTemp, AvgAirTemp) ->
    Now = os:timestamp(),
    OnPumpTemp = enigma_router_config:read_on_pump_temp(),
    OffPumpTemp = enigma_router_config:read_off_pump_temp(),
    OnPumpDelay = enigma_router_config:read_on_pump_delay(),
    OffPumpDelay = enigma_router_config:read_off_pump_delay(),
    OnFanTemp = enigma_router_config:read_on_fan_temp(),
    OffFanTemp = enigma_router_config:read_off_fan_temp(),
    OnFanDelay = enigma_router_config:read_on_fan_delay(),
    OffFanDelay = enigma_router_config:read_off_fan_delay(),
    InversionOffset = enigma_router_config:read_inversion_offset(),
    error_logger:info_msg("new_alert_status: ~p ~p~n",
			  [{OnPumpTemp, OffPumpTemp, OnPumpDelay, OffPumpDelay},
                {OnFanTemp, OffFanTemp, OnFanDelay, OffFanDelay, InversionOffset}]),

    PumpSettings = {OnPumpTemp, OffPumpTemp, OnPumpDelay, OffPumpDelay},
    FanSettings = {OnFanTemp, OffFanTemp, OnFanDelay, OffFanDelay, InversionOffset},
    {NewPumpStatus, NewPumpSince} = get_pump_status(
        AvgGrdTemp,
        OldPumpStatus,    
        OldPumpSince,
        Now,                           
        PumpSettings),

    {NewFanStatus, NewFanSince} = get_fan_status(
        AvgGrdTemp,
        AvgAirTemp,
        OldFanStatus,    
        OldFanSince,
        Now,                           
        FanSettings
    ),
    
    Action = get_action_status(
        OldPumpStatus,
        NewPumpStatus,
        OldFanStatus,
        NewFanStatus
    ),
    %% return new state for this server + action to execute
    {{NewPumpStatus, NewFanStatus}, {NewPumpSince, NewFanSince}, Action}.



-spec get_pump_status(    
    GndTemp :: float(),
    Status :: frost_status(),
    Since :: time(),
    Now :: time(),                           
    PumpSettings :: pump_settings()
    ) ->
    {
        NewStatus :: frost_status(),
        NewSince :: time() %% time since the status CHANGED
    }.

get_pump_status(GndTemp, off, Since, _Now, {OnPumpTemp, _, _, _})
    when GndTemp < OnPumpTemp ->
        {starting, Since};

get_pump_status(GndTemp, starting, Since, Now, {OnPumpTemp, _, OnPumpDelay, _})
    when GndTemp < OnPumpTemp ->
    Alert = case time_elapsed(Now, Since) of
                T when T > OnPumpDelay -> frost;
                _Else -> starting
            end,
    {Alert, Now};

get_pump_status(GndTemp, frost, _Since, Now, {_, OffPumpTemp, _, _})
    when GndTemp >= OffPumpTemp ->
    {stopping, Now };

get_pump_status(GndTemp, stopping, Since, Now, {_, OffPumpTemp, _, OffPumpDelay})
    when GndTemp >= OffPumpTemp ->
    Alert = case time_elapsed(Now, Since) of
                T when T > OffPumpDelay -> off;
                _Else -> stopping
                end,
    {Alert, Now}.
 


-spec get_fan_status(
    GndTemp :: float(),
    InvTemp :: float(),
    Status :: frost_status(),
    Since :: time(),
    Now :: time(),                           
    FanSettings :: fan_settings()
    ) ->
    {
        NewStatus :: frost_status(),
        NewSince :: time() %% time since the status CHANGED
    }.
 
get_fan_status(GndTemp, AirTemp, off, Since, _Now,
    {OnFanTemp, _, _, _, AirTempOk})
    when GndTemp < OnFanTemp, AirTemp > (AirTempOk) ->
        {starting, Since};
 
get_fan_status(GndTemp, _AirTemp, starting, Since, Now,
    {OnFanTemp, _, OnFanDelay, _, _})
   when GndTemp < OnFanTemp ->
    Alert = case time_elapsed(Now, Since) of
                    T when T > OnFanDelay ->
                                    frost;
                    _Else -> starting
                end,
    {Alert, Now};
 
get_fan_status(GndTemp, _AirTemp, frost, _Since, Now,
   {_, OffFanTemp, _, _, _ })
    when GndTemp >= OffFanTemp ->
    {stopping, Now };
 
get_fan_status(GndTemp, _AirTemp, stopping, Since, Now,
    {_, OffFanTemp, _, OffFanDelay, _})
       when GndTemp >= OffFanTemp ->
    Alert = case time_elapsed(Now, Since) of
                    T when T > OffFanDelay ->
                                    off;
                    _Else -> stopping
                end,
    {Alert, Now}.
 
 
-spec get_action_status(
        PumpStatus :: frost_status(),
        NewPumpStatus :: frost_status(),
        FanStatus :: frost_status(),
        NewFanStatus :: frost_status()
    ) -> action().
%Pump is off and should turn on ----- (should not get straight into this state)
get_action_status(off, frost, _, _) ->
   pump_on_fan_off;
%Pump is off and should turn on
get_action_status(starting, frost, _, _) ->
   pump_on_fan_off;
%Pump is on and should turn off ----- (should not get straight into this state)
get_action_status(frost, off, _, _) ->
   pump_off_fan_off;
%Pump is on and should turn off
get_action_status(stopping, off, _, _) ->
   pump_off_fan_off;
%Pump is/will be off, Fan is off and should turn on ----- (should not get straight into this state)
get_action_status(off, off, off, frost) ->
   pump_off_fan_on;
%Pump is/will be off, Fan is off and should turn on
get_action_status(off, off, starting, frost) ->
   pump_off_fan_on;
%Pump is/will be off, Fan is on and should turn off  ----- (should not get straight into this state)
get_action_status(off, off, frost, off) ->
   pump_off_fan_off;
%Pump is/will be off, Fan is on and should turn off
get_action_status(off, off, stopping, off) ->
   pump_off_fan_off;
get_action_status(_,_,_,_) ->
   no_action.




execute(pump_off_fan_off) -> 
    enigma_router_main:stop_pump(),
    enigma_router_main:stop_fan();
execute(pump_on_fan_off) -> 
    enigma_router_main:start_pump(),
    enigma_router_main:stop_fan();
execute(pump_off_fan_on) -> 
    enigma_router_main:stop_pump(),
    enigma_router_main:start_fan();
execute(no_action) -> 
    ok.


% %% This assumes we get new data point fairly regularly with frequency
% %% at least superior to one every mininimum(ondelay, offdelay)
% %% otherwise we could trigger an action later than we should
% %% for instance: if we get a message every 5min and the temp is < OnTemp,
% %% because 5mn is > OnDelay we should action the pump, but instead this code
% %% will change the status to 'alert' and then action the pump
% %% when the next message arrives if temp is still < OnTemp

% -type time() :: {integer(), integer(), integer()}.
% -type pump_settings() :: {float(), float(), integer(), integer()}.
% -type fan_settings() :: {float(), float(), integer(), integer(), float()}.

% -spec get_alert_status(
%         PumpTemp :: float(),
%         FanTemp :: float(),
% 		Status :: all_status(), 
%         Since :: time(), 
%         Now :: time(),
% 		PumpSettings :: pump_settings(),
%         FanSettings :: fan_settings()
%     ) -> 
%     {NewStatus :: all_status(), %% status reflects whether the temperatures are about to trigger an action:
%                                 %% alert -> yes, if they stay like this for a given amount of time
%                                 %% ok -> no, temperatures are ok
%     NewSince :: time(), %% time since the status CHANGED
%     Action :: action() %% what we action (fan, pump)
%     }.


% %% 1.
% %% PumpTemp < on_fan_temp (for more than action_on_fan_delay) && 
% %% FanTemp > (PumpTemp + inversion_offset) && 
% %% PumpTemp > action_on_pump_temp
% %% -> turn fan on, turn pump off
% get_alert_status(PumpTemp, FanTemp, {alert, A2, A3, A4}, Since, Now,
% 	{OnPumpTemp, _, _, _}, 
%     {OnFanTemp, _, OnFanDelay, _, InversionOffset})
%     when PumpTemp < OnFanTemp, FanTemp > (PumpTemp + InversionOffset), PumpTemp > OnPumpTemp ->
%     Action = case time_elapsed(Now, Since) of
% 	       T when T > OnFanDelay ->
% 		   error_logger:info_msg("START FAN (pump ~wC, fan ~wC, for ~wsec) ~n",
% 					 [PumpTemp, FanTemp, T / 1000]),
% 		   pump_off_fan_on;
% 	       _Else -> no_action
% 	     end,
%     {{alert, A2, A3, A4}, Since, Action};

% %% 1.
% %% temperature could trigger an action, if they hold for t > OnFanDelay 
% %% (see clause above, for actual trigger)
% get_alert_status(PumpTemp, FanTemp, {ok, A2, A3, A4}, _Since, Now,
% 	{OnPumpTemp, _, _, _}, 
%     {OnFanTemp, _, _, _, InversionOffset})
%     when PumpTemp < OnFanTemp, FanTemp > (PumpTemp + InversionOffset), PumpTemp > OnPumpTemp ->
%     {{alert, A2, A3, A4}, Now, no_action};

% %% 1.
% %% if the conditions stop holding we downgrade 'alert' to 'ok'
% get_alert_status(PumpTemp, FanTemp, {alert, A2, A3, A4}, _Since, Now,
% 	{OnPumpTemp, _, _, _}, 
%     {OnFanTemp, _, _, _, InversionOffset})
%     when PumpTemp >= OnFanTemp; FanTemp =< (PumpTemp + InversionOffset); PumpTemp =< OnPumpTemp ->
%     {{ok, A2, A3, A4}, Now, no_action};



% %% 2.
% %% PumpTemp > action_off_fan for more than action_off_fan_delay 
% %% -> turn fan off, turn pump off
% get_alert_status(PumpTemp, _FanTemp, {A1, alert, A3, A4}, Since, Now,
%     _PumpSettings, 
%     {_, OffFanTemp, _, OffFanDelay, _})
%     when PumpTemp > OffFanTemp ->
%     Action = case time_elapsed(Now, Since) of
% 	    T when T > OffFanDelay ->
% 		    error_logger:info_msg("STOP BOTH (pump ~wC for ~wsec) ~n",
% 				[PumpTemp, T / 1000]),
% 		   pump_off_fan_off;
% 	    _Else -> no_action
% 	end,
%     {{A1, alert, A3, A4}, Since, Action};

% %% 2.
% %% temperature could trigger an action, if they hold for t > OffFanDelay 
% %% (see clause above, for actual trigger)
% get_alert_status(PumpTemp, _FanTemp, {A1, ok, A3, A4}, _Since, Now,
%     _PumpSettings, 
%     {_, OffFanTemp, _, _, _})
%     when PumpTemp > OffFanTemp ->
%     {{A1, alert, A3, A4}, Now, no_action};

% %% 2.
% %% if the conditions stop holding we downgrade 'alert' to 'ok'
% get_alert_status(PumpTemp, _FanTemp, {A1, alert, A3, A4}, _Since, Now,
%     _PumpSettings, 
%     {_, OffFanTemp, _, _, _})
%     when PumpTemp =< OffFanTemp ->
%     {{A1, ok, A3, A4}, Now, no_action};

% %% 3.
% %% PumpTemp < OnPumpTemp for more than OnPumpDelay
% %% -> turn pump on, turn fan off
% get_alert_status(PumpTemp, _FanTemp, {A1, A2, alert, A4}, Since, Now,
% 	{OnPumpTemp, _, OnPumpDelay, _}, 
%     _)
%     when PumpTemp < OnPumpTemp ->
%     Action = case time_elapsed(Now, Since) of
% 	       T when T > OnPumpDelay ->
% 		   error_logger:info_msg("START PUMP (pump ~wC for ~wsec)~n",
% 					 [PumpTemp, T / 1000]),
% 		   pump_on_fan_off;
% 	       _Else -> no_action
% 	     end,
%     {{A1, A2, alert, A4}, Since, Action}; %% SHOULD IT BE ok HERE ?

% %% 3.
% %% temperature could trigger an action, if they hold for t > OffFanDelay 
% %% (see clause above, for actual trigger)
% get_alert_status(PumpTemp, _FanTemp, {A1, A2, ok, A4}, _Since, Now,
% 	{OnPumpTemp, _, _, _}, 
%     _)
%     when PumpTemp < OnPumpTemp ->
%     {{A1, A2, alert, A4}, Now, no_action};

% %% 3.
% %% if the conditions stop holding we downgrade 'alert' to 'ok'
% get_alert_status(PumpTemp, _FanTemp, {A1, A2, alert, A4}, _Since, Now,
% 	{OnPumpTemp, _, OnPumpDelay, _}, 
%     _)
%     when PumpTemp >= OnPumpTemp ->
%     {{A1, A2, ok, A4}, Now, no_action};

% %% 4.
% %% If PumpTemp > OffPumpTemp for more than OffPumpDelay 
% %% -> turn pump off

% get_alert_status(PumpTemp, _FanTemp, {A1, A2, A3, alert}, Since, Now,
% 	   {_, OffPumpTemp, _, OffPumpDelay}, _)
%     when PumpTemp > OffPumpTemp ->
%     Action = case time_elapsed(Now, Since) of
% 	       T when T > OffPumpDelay ->
% 		   error_logger:info_msg("STOP PUMP (pump ~wC for ~wsec)~n",
% 					 [PumpTemp, T / 1000]),
% 		   pump_off_fan_off;
% 	       _Else -> no_action
% 	     end,
%     {{A1, A2, A3, alert}, Since, Action};

% %% 4.
% %% temperature could trigger an action, if they hold for t > OffPumpDelay 
% %% (see clause above, for actual trigger)
% get_alert_status(PumpTemp, _FanTemp, {A1, A2, A3, ok}, _Since, Now,
% 	   {_, OffPumpTemp, _, _}, _)
%     when PumpTemp > OffPumpTemp ->
%     {{A1, A2, A3, alert}, Now, no_action};

% %% 4.
% %% if the conditions stop holding we downgrade 'alert4' to 'ok'


% get_alert_status(PumpTemp, _FanTemp, {A1, A2, A3, alert}, Since, Now,
% 	   {_, OffPumpTemp, _, OffPumpDelay}, _)
%     when PumpTemp > OffPumpTemp ->
%     {{A1, A2, A3, ok}, Now, no_action};



% %% all the other situations means we keep the status as it was
% %% with the time the status changed.
% get_alert_status(_PumpTemp, _FanTemp, Status, Since, _, _, _) ->
%     error_logger:info_msg("STATUS IS SAME~n"),
%     {Status, Since, no_action}.





%%-spec time_elapsed(os::timestamp(), os::timestamp()) -> integer().
time_elapsed({Meg2, Sec2, Mil2}, {Meg1, Sec1, Mil1}) ->
    T1 = Meg1 * 1000000 + Sec1 * 1000 + Mil1,
    T2 = Meg2 * 1000000 + Sec2 * 1000 + Mil2,
    T2 - T1.

%% %% temperature at which point the frost prevention should turn on if we
%% %% have remained below the temperature for "action_pump_on_pump_delay" seconds.
%% {<<"action_pump_on_pump_temp">>, 0},

%% %% only turn on if we have been below "action_pump_on_pump_temp" for x seconds (2 min)
%% {<<"action_pump_on_delay">>, 120},

%% %% temperature at which point the frost prevention should turn off if we
%% %% have remained above the temperature for "action_off_pump_delay" seconds.
%% {<<"action_off_pump_temp">>, 2},

%% %% only turn off if we have been above "action_off_pump_temp" for x seconds (5 min)
%% {<<"action_off_pump_delay">>, 300}

%% Get the temperatures for the pump
get_pump_temps(Data) -> get_dt(0, Data).

%% Get the temperatures for the fan
get_fan_temps(Data) -> get_dt(5, Data).

%% extract measurements for a given DT_Code from a parsed sensor payload
get_dt(_, []) -> [];
get_dt(DT_Code,
       [{_SensorId, _SensorNum, DT_Code, Val} | Rest]) ->
    [Val | get_dt(DT_Code, Rest)];
get_dt(DT_Code, [_ | Rest]) -> get_dt(DT_Code, Rest).

average_nine(Nums) when length(Nums) < 9 ->
    average(Nums);
average_nine(Nums) when length(Nums) == 9 ->
    Nums2 = lists:sublist(Nums, 4, 3), average(Nums2);
average_nine(Nums) -> average(lists:sublist(Nums, 9)).

average([]) -> 30; %% nice temperature to start with
average(Nums) -> lists:sum(Nums) / length(Nums).
