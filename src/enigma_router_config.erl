%%%-------------------------------------------------------------------
%%% @author manu <manu@chleb>
%%% @copyright (C) 2012, manu
%%% @doc
%%% @end
%%% TODO: keep config in the server state as well....
%%% Created :  3 Apr 2012 by manu <manu@chleb>
%%%-------------------------------------------------------------------

-module(enigma_router_config).

-behaviour(gen_server).

%% API
-export([read_config/0, read_inversion_offset/0,
	 read_off_fan_delay/0, read_off_fan_temp/0,
	 read_off_pump_delay/0, read_off_pump_temp/0,
	 read_on_fan_delay/0, read_on_fan_temp/0,
	 read_on_pump_delay/0, read_on_pump_temp/0, start_link/0,
	 write_config/1]).

%%         get_host/0

-include("enigma_router.hrl").

-define(CONFIG, 'db/config.db').

-define(SEC_DAY, 86400).

%% gen_server callbacks
-export([code_change/3, handle_call/3, handle_cast/2,
	 handle_info/2, init/1, terminate/2]).

-include_lib("eunit/include/eunit.hrl").

%%%===================================================================
%%% API
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc Starts the server
%%--------------------------------------------------------------------
-spec start_link() -> {ok, pid()} | ignore |
		      {error, any()}.

start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, [],
			  []).

%%--------------------------------------------------------------------
%% @doc Return config version
%%--------------------------------------------------------------------
%% -spec config_version() -> pos_integer() | {error, atom()}.
%% config_version() ->
%%     gen_server:call(?MODULE, config_version).
%%
%%--------------------------------------------------------------------
%% @doc Write config
%%--------------------------------------------------------------------
-spec write_config(config()) -> ok.

write_config(Config) ->
    gen_server:cast(?MODULE, {write_config, Config}).

%%--------------------------------------------------------------------
%% @doc Read config
%%--------------------------------------------------------------------
-spec read_config() -> config().

read_config() -> gen_server:call(?MODULE, read_config).

%%--------------------------------------------------------------------
%% @doc Read on_pump_temp
%%--------------------------------------------------------------------
-spec read_on_pump_temp() -> float().

read_on_pump_temp() ->
    gen_server:call(?MODULE, read_on_pump_temp).

%%--------------------------------------------------------------------
%% @doc Read off_pump_temp
%%--------------------------------------------------------------------
-spec read_off_pump_temp() -> float().

read_off_pump_temp() ->
    gen_server:call(?MODULE, read_off_pump_temp).

%%--------------------------------------------------------------------
%% @doc Read on_pump_delay
%%--------------------------------------------------------------------
-spec read_on_pump_delay() -> integer().

read_on_pump_delay() ->
    gen_server:call(?MODULE, read_on_pump_delay).

%%--------------------------------------------------------------------
%% @doc Read off_pump_temp
%%--------------------------------------------------------------------
-spec read_off_pump_delay() -> integer().

read_off_pump_delay() ->
    gen_server:call(?MODULE, read_off_pump_delay).

%%--------------------------------------------------------------------
%% @doc Read inversion_offset
%%--------------------------------------------------------------------
-spec read_inversion_offset() -> float().

read_inversion_offset() ->
    gen_server:call(?MODULE, read_inversion_offset).

%%--------------------------------------------------------------------
%% @doc Read on_fan_temp
%%--------------------------------------------------------------------
-spec read_on_fan_temp() -> float().

read_on_fan_temp() ->
    gen_server:call(?MODULE, read_on_fan_temp).

%%--------------------------------------------------------------------
%% @doc Read off_fan_temp
%%--------------------------------------------------------------------
-spec read_off_fan_temp() -> float().

read_off_fan_temp() ->
    gen_server:call(?MODULE, read_off_fan_temp).

%%--------------------------------------------------------------------
%% @doc Read on_fan_delay
%%--------------------------------------------------------------------
-spec read_on_fan_delay() -> integer().

read_on_fan_delay() ->
    gen_server:call(?MODULE, read_on_fan_delay).

%%--------------------------------------------------------------------
%% @doc Read off_fan_temp
%%--------------------------------------------------------------------
-spec read_off_fan_delay() -> integer().

read_off_fan_delay() ->
    gen_server:call(?MODULE, read_off_fan_delay).

%%--------------------------------------------------------------------
%% @doc Get online URL and Port
%%--------------------------------------------------------------------
%% -spec get_host() -> {string(), string()}.
%% get_host() ->
%%     gen_server:call(?MODULE, get_host).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Initializes the server
%%
%% @spec init(Args) -> {ok, State} |
%%                     {ok, State, Timeout} |
%%                     ignore |
%%                     {stop, Reason}
%% @end
%%--------------------------------------------------------------------
init([]) ->
    {ok, ?CONFIG} = dets:open_file(?CONFIG,
				   [{type, set}, {keypos, 1}]),
    Config = init_config(),
    {ok, Config}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling call messages
%%
%% @spec handle_call(Request, From, State) ->
%%                                   {reply, Reply, State} |
%%                                   {reply, Reply, State, Timeout} |
%%                                   {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, Reply, State} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
%% @doc Return the config
handle_call(read_config, _From, State) ->
    %% Reply = select_all(),
    {reply, State, State};

handle_call(read_on_pump_temp, _From, State) ->
    %%error_logger:info_msg("State: ~w, look:~w~n",
    %%		  [State, proplists:lookup(on_pump_temp, State)]),
    {on_pump_temp, Reply} = proplists:lookup(on_pump_temp, State),
    {reply, Reply, State};

handle_call(read_off_pump_temp, _From, State) ->
    {off_pump_temp, Reply} = proplists:lookup(off_pump_temp, State),
    {reply, Reply, State};

handle_call(read_on_pump_delay, _From, State) ->
    {on_pump_delay, Reply} = proplists:lookup(on_pump_delay, State),
    {reply, Reply, State};

handle_call(read_off_pump_delay, _From, State) ->
    {off_pump_delay, Reply} = proplists:lookup(off_pump_delay, State),
    {reply, Reply, State};

handle_call(read_on_fan_temp, _From, State) ->
    {on_fan_temp, Reply} = proplists:lookup(on_fan_temp, State),
    {reply, Reply, State};

handle_call(read_off_fan_temp, _From, State) ->
    {off_fan_temp, Reply} = proplists:lookup(off_fan_temp, State),
    {reply, Reply, State};

handle_call(read_on_fan_delay, _From, State) ->
    {on_fan_delay, Reply} = proplists:lookup(on_fan_delay, State),
    {reply, Reply, State};

handle_call(read_off_fan_delay, _From, State) ->
    {off_fan_delay, Reply} = proplists:lookup(off_fan_delay, State),
    {reply, Reply, State};

handle_call(read_inversion_offset, _From, State) ->
    {inversion_offset, Reply} = proplists:lookup(inversion_offset, State),
    {reply, Reply, State};
    
handle_call(_Request, _From, State) ->
    Reply = ok, {reply, Reply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling cast messages
%%
%% @spec handle_cast(Msg, State) -> {noreply, State} |
%%                                  {noreply, State, Timeout} |
%%                                  {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_cast({write_config, Config}, _State) ->
    %% error_logger:info_msg("write config:~p~n", [Config]),
    ok = dets:insert(?CONFIG,
		     Config), %% on disk for persistence
    {noreply, Config};
handle_cast(_Msg, State) -> {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling all non call/cast messages
%%
%% @spec handle_info(Info, State) -> {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_info(_Info, State) -> {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_server terminates
%% with Reason. The return value is ignored.
%%
%% @spec terminate(Reason, State) -> void()
%% @end
%%--------------------------------------------------------------------
terminate(_Reason, _State) -> ok = dets:close(?CONFIG).

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Convert process state when code is changed
%%
%% @spec code_change(OldVsn, State, Extra) -> {ok, NewState}
%% @end
%%--------------------------------------------------------------------
code_change(_OldVsn, State, _Extra) -> {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================

%%% Config functions

%%% retrieve the existing config stored on disk
%%% or initialize it to a default config, if it does not exist yet
-spec init_config() -> config().

init_config() ->
    case dets:first(?CONFIG) of
      '$end_of_table' ->
	  DefaultConfig = default_config(),
	  ok = dets:insert(?CONFIG, DefaultConfig),
	  DefaultConfig;
      _Else -> dets:select(?CONFIG, [{'_', [], ['$_']}])
    end.

-spec default_config() -> config().

default_config() ->
    [{on_pump_temp, 0.0}, {off_pump_temp, 2.0},
     {on_pump_delay, 120}, {off_pump_delay, 300},
     {on_fan_temp, 0.0}, {off_fan_temp, 2.0},
     {on_fan_delay, 120}, {off_fan_delay, 300},
     {inversion_offset, 5.0}].

%% empty_config() ->
%%     {<<"config">>,
%%      [%%  when temp is >= "gprs_datarate_temp" send data every x seconds (30 min)
%%       {<<"gprs_datarate_slow">>,1800},

%%       %% when temp is < "gprs_datarate_temp" send data every x seconds (5 min)
%%       {<<"gprs_datarate_fast">>, 300},

%%       %% temperature in degrees
%%       {<<"gprs_datarate_temp">>, 5},

%%       %% temperature at which point the frost prevention should turn on if we
%%       %% have remained below the temperature for "action_on_pump_delay" seconds.
%%       {<<"action_on_pump_temp">>, 0},

%%       %% only turn on if we have been below "action_on_pump_temp" for x seconds (2 min)
%%       {<<"action_on_pump_delay">>, 120},

%%       %% temperature at which point the frost prevention should turn off if we
%%       %% have remained above the temperature for "action_off_pump_delay" seconds.
%%       {<<"action_off_pump_temp">>, 2},

%%       %% only turn off if we have been above "action_off_pump_temp" for x seconds (5 min)
%%       {<<"action_off_pump_delay">>, 300}
%%      ]

%%      %% url + port of online tcp server
%%      %% <<"host">>, {"50.116.12.207", "5555"}
%%     }.

