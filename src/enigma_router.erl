-module(enigma_router).


-export([start/0]).

%% ===================================================================
%% Application callbacks
%% ===================================================================

start() ->
    ok = application:start('erlang-serial'),
    ok = application:start(gpio),
    ok = application:start(ranch),
    ok = application:start(crypto),
    ok = application:start(cowboy),
    ok = application:start(enigma_router).


