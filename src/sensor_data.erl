%%%-------------------------------------------------------------------
%%% @author manu
%%% @doc This module defines functions to help manipulate sensor data
%%% @end
%%% Created : 30 Jul 2013 by manu
%%%-------------------------------------------------------------------
-module(sensor_data).

-export([get_sensordata/1,
         get_sensor_readings/1,
         get_sensordata/2,
         get_sensordata/3,
         get_scaled/2]).





%% %% @doc returns the external temperature from a sensor data packet
%% -spec get_etemp(Data::binary()) -> {ok, integer()} | {error, not_found} | no_return().
%% get_etemp(<<_SensorId:32, Rest/binary>>) ->
%%     %% search for a null byte (the value for ext temp tag)
%%     lookup_sensor_dt(0, Rest);
%% get_etemp(_) -> error(invalid_sensor_data).
%% 
%% 
%% %% @doc Lookup for all values
%% -spec lookup_sensor_dt(DT::integer(), Data::binary()) -> {ok, integer()} | {error, not_found}.
%% lookup_sensor_dt(_, <<>>) -> 
%%     {error, not_found};    
%% lookup_sensor_dt(DT, <<DT:8, Val1:16/integer-little, Rest/binary>>) -> %% we hardcode that val is on 2 bytes
%%     lager:info("Found: ~p continuing...~n",[Val1]),
%%     lookup_sensor_dt(DT, Val1, Rest);
%% lookup_sensor_dt(DT, <<_NDT:8, Rest/binary>>) ->
%%     lookup_sensor_dt(DT, Rest).
%% 
%% 
%% -spec lookup_sensor_dt(DT::integer(), Default::integer(), Data::binary()) -> {ok, integer()}.
%% lookup_sensor_dt(_, Val, <<>>) ->
%%     lager:info("Found: ~p~n",[Val]),
%%     {ok, Val};	
%% lookup_sensor_dt(DT, Val1, <<DT:8, Val2:16/integer-little, _Rest/binary>>) -> %% we hardcode that val is on 2 bytes
%%     lager:info("Found: ~p, ~p~n",[Val1, Val2]),
%%     {ok, (Val1+Val2)/2};
%% lookup_sensor_dt(DT, Val1, <<_NDT:8, Rest/binary>>) ->
%%     lager:info("With: ~p continuing~n",[Val1]),
%%     lookup_sensor_dt(DT, Val1, Rest).


%% @doc Given a data packet, returns battery, temperature and humidity
-spec get_sensordata(Data::binary()) -> {'ok',non_neg_integer(),{char(),char(),char()}}.
get_sensordata(<<Sensor:32/integer-little, Rest/binary>>) ->
    %% search for a 4 byte (the value for battery tag)
    {ok, Battery} = lookup_sensor_2byte(4, Rest),
    {ok, Temp} = lookup_sensor_2byte(0, Rest),
    {ok, RelHum} = lookup_sensor_2byte(2, Rest),
    {ok, Sensor, {Battery, Temp, RelHum}};
get_sensordata(_) -> error(invalid_sensor_data).


%% @doc Given a data packet, returns battery, temperature and humidity
-spec get_sensor_readings(Data::binary()) -> {'ok',non_neg_integer(), [{0|2|4, char()}]}.
get_sensor_readings(<<Sensor:32/integer-little, Rest/binary>>) ->
    %% search for a 4 byte (the value for battery tag)
    {ok, Battery} = lookup_sensor_2byte(4, Rest),
    {ok, Temp} = lookup_sensor_2byte(0, Rest),
    {ok, RelHum} = lookup_sensor_2byte(2, Rest),
    {ok, Sensor, [{4,Battery}, {0,Temp}, {2,RelHum}]};
get_sensor_readings(_) -> error(invalid_sensor_data).




%% @doc Given a sensor id and data packet, 
%% returns battery, temperature and humidity values if data packet comes from this sensor
%% 4> sensor_data:get_sensordata(<<1,0,0,0,0,5,0,2,6,0,4,7,0>>).    
%% {ok,1,{7,5,6}}
%% @end
-spec get_sensordata(SensorId::integer(), binary()) -> {ok, integer(), tuple()} | {error, not_found} | no_return().
get_sensordata(Sensor, <<Sensor:32/integer-little, Rest/binary>>) ->
    %% search for a 4 byte (the value for battery tag)
    {ok, Battery} = lookup_sensor_2byte(4, Rest),
    {ok, Temp} = lookup_sensor_2byte(0, Rest),
    {ok, RelHum} = lookup_sensor_2byte(2, Rest),
    {ok, Sensor, {Battery, Temp, RelHum}};
get_sensordata(_,_) ->
    {error, not_found}.


%% @doc Given a sensor id a DataType and a data packet, 
%% returns the first value found, if data packet comes from this sensor
%% @end
-spec get_sensordata(integer(), integer(), binary()) -> {ok, integer(), integer()} | {error, not_found} | no_return().
get_sensordata(Sensor, Datatype, <<Sensor:32/integer-little, Rest/binary>>) ->
    {ok, Value} = lookup_sensor_2byte(Datatype, Rest),
    {ok, Sensor, Value};
get_sensordata(_,_,_) ->
    {error, not_found}.


%% @doc retrieves the first 2 bytes value, found for a given DataType
-spec lookup_sensor_2byte(DT::integer(), Data::binary()) -> {ok, integer()} | {error, not_found}.
lookup_sensor_2byte(_, <<>>) -> 
    {error, not_found};
lookup_sensor_2byte(DT, <<DT:8, Val:16/integer-little, _Rest/binary>>) -> %% we hardcode that val is on 2 bytes
    {ok, Val};
lookup_sensor_2byte(DT, <<_NDT:8, Rest/binary>>) ->
    lookup_sensor_2byte(DT, Rest).



%% @doc Given a sensor payload, returns the first value found, scaled.
%% @end
-spec get_scaled(integer(), binary()) -> {ok, integer(), float()} | no_return().
get_scaled(DataType, <<Sensor:32/integer-little, Rest/binary>>) ->
    {ok, Value} = lookup_sensor_2byte(DataType, Rest),
    Scaled = case DataType of
               3 -> Value;
               _ -> Value / 100
             end,  
    {ok, Sensor, Scaled}.


