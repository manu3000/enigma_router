%%%-------------------------------------------------------------------
%%% @author manu
%%% @doc This module handles the server-side logic for the
%%% router dashboard webapp 
%%% @end
%%% Created : 30 Jul 2013 by manu
%%%-------------------------------------------------------------------
-module(ws_handler).

-behaviour(cowboy_websocket_handler).
-compile([{parse_transform, lager_transform}]).

-export([init/3]).
-export([websocket_init/3]).
-export([websocket_handle/3]).
-export([websocket_info/3]).
-export([websocket_terminate/3]).


-define(PUSH_RATE, 5000).
-define(UPDATE_RATE, 20000).

%% -record(st, {}).

init({tcp, http}, _Req, _Opts) ->
    {upgrade, protocol, cowboy_websocket}.

websocket_init(_TransportName, Req, _Opts) ->
    erlang:start_timer(100, self(), saved_config),
    erlang:start_timer(100, self(), router_status),
    erlang:start_timer(100, self(), signal_strength),
    erlang:start_timer(100, self(), ext_temp),
    {ok, Req, no_state}.

%%  Cowboy handler ws_handler terminated in ws_handler:websocket_handle/3 with reason: bad argument in jsx_parser:value/4 line 125
%%  Txt = <<"{\"sensordata\":{\"sensor_id\":1,\"data_type\":4}}">>
%%  Query = [{<<"sensordata">>, [{<<"sensor_id">>,1},{<<"data_type">>,4}]}]
websocket_handle({text, Txt}, Req, State) ->
    error_logger:info_msg("ws text:~w~n", [Txt]),
    Query = jsx:decode(Txt),
    error_logger:info_msg("ws query:~w~n", [Query]),
    Resp = handle_request(Query),
    error_logger:info_msg("ws response:~w~n", [Resp]),
    {reply, {text, Resp}, Req, State};
    %% {reply, {text, jsx:encode([{<<"msg">>, <<"Config saved">>}])}, Req, State};
websocket_handle(_Data, Req, State) ->
    {ok, Req, State}.

websocket_info({timeout, _Ref, saved_config}, Req, S) ->
    erlang:start_timer(?UPDATE_RATE, self(), saved_config),
    Conf = enigma_router_config:read_config(),
    Msg = jsx:encode([{config, Conf}]),  %% <<"{\"config\": { more_stuff_here }}">>
    {reply, {text, Msg}, Req, S};
websocket_info({timeout, _Ref, router_status}, Req, S) ->
    erlang:start_timer(?PUSH_RATE, self(), router_status),
    <<_:4, AT_Ready:1, Status_ON:1, Status_ERR:1, Status_ACT:1>> = enigma_router_main:router_status(),
    Status = [{<<"power">>, Status_ACT}, {<<"error">>, Status_ERR}, {<<"running">>, Status_ON}, {<<"modem">>, AT_Ready}],
    %% error_logger:info_msg("Router status is ~w, ~w, ~w~n",[Status_ON, Status_ERR, Status_ACT]),
    Msg = jsx:encode([{<<"router_status">>, Status}]),
    {reply, {text, Msg}, Req, S};
websocket_info({timeout, _Ref, signal_strength}, Req, S) ->
    erlang:start_timer(?PUSH_RATE, self(), signal_strength),
    Signal = get_signal_strength(),
    Msg = jsx:encode([{<<"signal">>, Signal}]),
    {reply, {text, Msg}, Req, S};
websocket_info({timeout, _Ref, ext_temp}, Req, S) ->
    erlang:start_timer(?PUSH_RATE, self(), ext_temp),
    %% Msg = handle_request(<<"etemps">>), %% problem with enigma_router_data:readings() needs to be fixed first
    AlertPumpTemp = enigma_router_alert:avg_pump_temp(),
    Msg = jsx:encode([{<<"etemp">>, AlertPumpTemp }]),
    {reply, {text, Msg}, Req, S};
websocket_info({timeout, _Ref, air_temp}, Req, S) ->
    erlang:start_timer(?PUSH_RATE, self(), air_temp),
    AlertFanTemp = enigma_router_alert:avg_fan_temp(),
    Msg = jsx:encode([{<<"ftemp">>, AlertFanTemp }]),
    {reply, {text, Msg}, Req, S};
websocket_info(_Info, Req, State) ->
    {ok, Req, State}.

websocket_terminate(_Reason, _Req, _State) ->
    ok.


%%%===================================================================
%%% Internal functions
%%%===================================================================

handle_request([{<<"config">>, Config}]) ->
    %% atomize !
    Config2 = [{binary_to_atom(B, latin1), V} || {B,V} <- Config] ,
    %% save config 
    enigma_router_config:write_config(Config2),
    %% send confirmation message
    jsx:encode([{<<"msg">>, <<"Config saved">>}]);

%% handle_request(<<"config">>) ->
%%      Conf = enigma_router_config:read_config(),
%%     jsx:encode([{config, Conf}]);

%% handle_request(<<"router_status">>) ->
%%     error_logger:info_msg("Router status has been requested..."),
%%     <<0:5, Status_ON:1, Status_ERR:1, Status_ACT:1>> = enigma_router_main:router_status(),
%%     Status = [{<<"power">>, Status_ACT}, {<<"error">>, Status_ERR}, {<<"running">>, Status_ON}],
%%     error_logger:info_msg("Router status is ~w, ~w, ~w~n",[Status_ON, Status_ERR, Status_ACT]),
%%     jsx:encode([{<<"router_status">>, Status}]);

handle_request(<<"stations">>) ->
	error_logger:info_msg("Requested stations ~n"),
	Stations = enigma_router_data:stations(),
	error_logger:info_msg("Stations read ~p~n",[Stations]),
	jsx:encode([{<<"stations">>, Stations}]);

%% {"sensordata":{"sensor_id":1,"data_type":4}}
%% [{<<"sensordata">>, [{<<"sensor_id">>,1},{<<"data_type">>,4}]}]
handle_request([{<<"sensordata">>, [{<<"sensor_id">>, SensorId},{<<"data_type">>,DT_Code}]}]) ->
    Data = enigma_router_data:readings(SensorId, DT_Code),
    error_logger:info_msg("data read ~p~n",[Data]),
    %% {DateTime, SensorId, SensorNum, DT_Code, Val}
    Data2 = [to_proplist(Tup) || Tup <- Data],
        error_logger:info_msg("data fmted ~p~n",[Data2]),
    jsx:encode([{<<"type">>, DT_Code}, {<<"sensordata">>, Data2}]).


%% handle_request(<<"etemps">>) ->
%%     ETemp = enigma_router_data:readings(0), %% DT_Code = 0 => temperatures
%%     %% {DateTime, SensorId, SensorNum, DT_Code, Val}
%%     ETemp2 = [[{<<"time">>, calendar:now_to_datetime(Time)},
%%                {<<"sensor_id">>, SensorId},
%%                {<<"sensor_num">>, SensorNum},
%%                {<<"temp">>, Temp}] || {Time, SensorId, SensorNum, _DT, Temp} <- ETemp],
%%     jsx:encode([{<<"etemps">>, ETemp2}]).
%% 


%% handle_request(<<"sensordata">>) ->
%%     EData = get_24h_sensordata(),
%%     EData2 = [[{<<"time">>, Hours}, {<<"sensor">>, Sensor}, {<<"battery">>, Battery}, 
%%                {<<"temp">>, Temp}, {<<"rh">>, RelHum}] || 
%%                  {Hours, Sensor, Battery, Temp, RelHum} <- EData],
%%     jsx:encode([{<<"sensordata">>, EData2}]);

%% handle_request([{<<"sensordata">>,[{<<"sensor_id">>, Sid}, {<<"data_type">>, DT}]}]) ->
%%     EData = get_24h_sensordata(Sid, DT),
%%     EData2 = [[{<<"time">>, Hours}, {<<"sensor">>, Sensor}, {<<"value">>, Value}] || 
%%                  {Hours, Sensor, Value} <- EData],
%%     jsx:encode([{<<"sensordata">>, EData2},{<<"type">>, DT}]).



%% handle_request(<<"signal_strength">>) ->
%%     Signal = get_signal_strength(),
%%     jsx:encode([{<<"signal">>, Signal}]).


%% return a tuple {Hour, Sensor, DATA}, where
%% Hour is the number of (negative) hours since the temp was recorded
%% Sensor is the id of the sensor
%% DATA is a tuple with {Battery Level, Temperature, Relative Humidty}
%% get_24h_sensordata() ->
%%     Readings = enigma_router_data:readings(),
%%     OrderedReadings = lists:keysort(1, Readings),
%%     DataReadings = 
%%         lists:foldl(
%%           fun({Time,Data}, Acc) -> 
%%                  {ok, Sensor, {Battery, Temp, RelHum}} = 
%%                       sensor_data:get_sensordata(Data),
%%                   [{Time, Sensor, Battery, Temp, RelHum}|Acc]
%%           end, [], OrderedReadings),
    
%%    	{_, S1, _} = now(),
%%     [{(S1-S2)/3600, S, B/100, T/100, RH/100} || 
%%         {{_, S2, _}, S, B, T, RH} <- DataReadings].    


%% get_24h_sensordata(Sensor) ->
%%     Readings = enigma_router_data:readings(),
%%     OrderedReadings = lists:keysort(1, Readings),
%%     DataReadings = lists:foldl(
%%                      fun({Time,Data}, Acc) -> 
%%                              case sensor_data:get_sensordata(Sensor, Data) of
%%                                  {ok, Sensor, {Battery, Temp, RelHum}} -> 
%%                                      [{Time, Sensor, Battery, Temp, RelHum}|Acc];
%%                                  {error, _} -> Acc
%%                              end
%%                      end, [], OrderedReadings),
%% 
%%    	{_, S1, _} = now(),
%%     [{(S1-S2)/3600, S, B/100, T/100, RH/100} || {{_, S2, _}, S, B, T, RH} <- DataReadings].      

%% get_24h_sensordata(Sensor, Datatype) ->
%%     Readings = enigma_router_data:readings(),
%%     OrderedReadings = lists:keysort(1, Readings),
%%     DataReadings = 
%%         lists:foldl(
%%           fun({Time,Data}, Acc) -> 
%%                   case sensor_data:get_sensordata(Sensor, Datatype, Data) of
%%                       {ok, Sensor, Value} -> [{Time, Sensor, Value}|Acc];
%%                       {error, _} -> Acc
%%                   end
%%           end, [], OrderedReadings),
%%     
%%    	{_, S1, _} = now(),
%%     [{(S1-S2)/3600, S, V} || {{_, S2, _}, S, V} <- DataReadings].                    

%% %% return a tuple {Hour, Temp}, where
%% %% Hour is the number of (negative) hours since the temp was recorded 
%% %% Temp is the temperature recorded scaled down (by 100)
%% get_24h_etemp() ->
%%     Readings = enigma_router_data:readings(),
%%     TempReadings = lists:keysort(1, Readings),
%%     TempReadings2 = lists:foldl(
%%                       fun({Time,Data}, Acc) -> 
%%                               case sensor_data:get_etemp(Data) of
%%                                   {ok, Temp} -> [{Time, Temp}|Acc];
%%                                   {error, _} -> Acc
%%                               end
%%                       end, [], TempReadings),

%%     {_, S1, _} = now(),
%%     [{(S1-S2)/3600, T/100} || {{_, S2, _}, T} <- TempReadings2].           


get_signal_strength() ->
  try enigma_router_modem:signal_strength() of
        N -> N
   catch
       _:_ ->  <<"undefined">>
 end.

to_proplist({Time, StationId, SensorNum, _DT_Code, Val}) ->
  {{Y,Mo,D},{H,Mi,S}} = calendar:now_to_datetime(Time),		   
  [{<<"time">>, [Y,Mo,D,H,Mi,S]},
    {<<"sensor_id">>, StationId},
    {<<"sensor_num">>, SensorNum},
    {<<"value">>, Val}].
