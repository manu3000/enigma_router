%% start the VM
%% sudo erl -pa ebin -pa deps/*/ebin
%% initialize the serial port com
%% Serial = serial:start([{speed,115200},{open,"/dev/ttyAMA0"}]).
%% starting / stopping the modem ?
%% modem_tes:power_on() or power_off() or reset() 
%% sending an AT msg to modem
%% modem_test:send(Serial, <<"AT\r\n">>).


%% power_on, power_off worked after clean start

-module(modem_test).

-compile(export_all).


-define(DEVICE, "/dev/ttyAMA0").
-define(ONKEY, 7).
-define(DSR_CTS, 8).

main() ->
    dbg:start(),
    dbg:tracer(),
    application:start('erlang-serial'),
    application:start('gpio'),
    Serial = serial:start([{speed,115200},{open,?DEVICE}]),
    dbg:p(Serial,[c,m]),
    gpio_init(),
%    power_on(),
%    power_off(),
    Serial.
    % send(Serial, <<"AT\r\n">>),
    % spawn(fun() -> loop(Serial) end).

loop(Serial) ->
    receive
        at ->
            send(Serial, <<"AT\r\n">>), %% worked
            loop(Serial)
    end.


%%-------------------------------------------------------------
get_serial() ->
    serial:start([{speed,115200},{open,"/dev/ttyAMA0"}]).

at(Serial) ->
    modem_test:send(Serial, <<"AT\r\n">>).

gpio_init() ->
    A = gpio:release(?ONKEY),
    B = gpio:init(?ONKEY),
    C = gpio:set_direction(?ONKEY, low),
    D = gpio:release(?DSR_CTS),
    E = gpio:init(?DSR_CTS),
    F = gpio:set_direction(?DSR_CTS, high),
    error_logger:info_msg("release onkey:~p~ninitdirect onkey:~p~n"
			  "setdir onkey:~p~nrelease dsr:~p~n"
			  "initdirect dsr:~p~nsetdir:~p~n",[A,B,C,D,E,F]).


%% simple approach to restarting modem when not responsive
reset() ->
    power_off(),
    power_on().


%% other approach to restarting modem when not responsive
%% check state of ONKEY and DSR_CTS pins first...
reset2() ->
    {ok, OnKey} = gpio:get(?ONKEY), %% {ok,uint1()} | {error,Reason::posix()}.
    {ok, Dsr} = gpio:get(?DSR_CTS),
    %% error_logger:info_msg("ONKEY: ~p, DSR: ~p~n",[OnKey,Dsr]),
    case {OnKey, Dsr} of
        %% {0, 0} -> do_nothing;
        {0, 1} -> power_on(); 
        {_, _} -> power_off(), power_on()
    end.


power_on() ->
    error_logger:info_msg("Modem: Powering Up - START~n"),
    %% error_logger:info_msg("Modem: ONKEY=Low, DSR/CTS=High~n"),
    timer:sleep(500), %% t1 > 200ms
    ok = gpio:set(?ONKEY),
    %% error_logger:info_msg("Modem: ONKEY=High, DSR/CTS=High~n"),
    timer:sleep(2500), %% t2 > 2000ms
    ok = gpio:clr(?ONKEY),
    %% error_logger:info_msg("Modem: ONKEY=LOW, DSR/CTS=High~n"),
    timer:sleep(3000), %% t3 > 2500ms
    ok = gpio:set(?DSR_CTS),
    %% error_logger:info_msg("Modem: ONKEY=Low, DSR/CTS=Low~n"),
    timer:sleep(200),  %% t4 > 100ms
    error_logger:info_msg("Modem: Powering Up - END - WAIT 15sec~n"),
    timer:sleep(15000). %% 15 sec


power_off() ->
    error_logger:info_msg("Modem: Powering Down - START~n"),
    %% error_logger:info_msg("Modem: ONKEY=Low, DSR/CTS=High~n"),
    ok = gpio:clr(?ONKEY),
    ok = gpio:clr(?DSR_CTS),
    timer:sleep(500), %% t1 > 200ms
    %% error_logger:info_msg("Modem: ONKEY=High, DSR/CTS=High~n"),
    ok = gpio:set(?ONKEY),
    timer:sleep(2500), %% t2 > 2000ms
    %% error_logger:info_msg("Modem: ONKEY=LOW, DSR/CTS=High - WAIT 6.5sec~n"),
    ok = gpio:clr(?ONKEY),
    timer:sleep(6500), %% t3 > 6000ms
    error_logger:info_msg("Modem: Powering Down - END - WAIT 15sec~n"),
    timer:sleep(15000). %% 15 sec


send(Serial, Command) ->
    error_logger:info_msg("send command: ~p~n",[Command]),
    Serial ! {send, Command},
    receive_loop().

receive_loop() ->
    receive
        Data -> error_logger:info_msg("modem responded: ~p~n",[Data]),
        receive_loop()
    after
        3000 ->
            error_logger:info_msg("modem silent for 3sec~n")
    end.
