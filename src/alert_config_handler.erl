%% curl -i -H "Accept: application/json" http://localhost:8080/alert_config

-module(alert_config_handler).
-behavior(cowboy_http_handler).

-include("enigma_router.hrl").
 
-export([init/3]).
-export([handle/2]).
-export([terminate/3]).
 
init(_Type, Req, _Opts) ->
    {ok, Req, undefined_state}.
 

handle(Req, State) ->
    {Method, Req2} = cowboy_req:method(Req),
    {ok, Req3} = config(Method, Req2),
    {ok, Req3, State}.
 
config(<<"GET">>, Req) ->
    Config = enigma_router_config:read_config(),
    Body = jsx:encode(Config),
    cowboy_req:reply(200, [{<<"content-type">>,
                            <<"application/json">>}
                          ], [Body], Req);
config(<<"PUT">>, Req) -> 
    %% HasBody = cowboy_req:has_body(Req),
    {ok, PostVals, Req2} = cowboy_req:body_qs(Req),
    OnPumpTemp = proplists:get_value(<<"on_pump_temp">>, PostVals),
    OffPumpTemp = proplists:get_value(<<"off_pump_temp">>, PostVals),
    OnPumpDelay = proplists:get_value(<<"on_pump_delay">>, PostVals),
    OffPumpDelay = proplists:get_value(<<"off_pump_delay">>, PostVals),
    OnFanTemp = proplists:get_value(<<"on_fan_temp">>, PostVals),
    OffFanTemp = proplists:get_value(<<"off_fan_temp">>, PostVals),
    OnFanDelay = proplists:get_value(<<"on_fan_delay">>, PostVals),
    OffFanDelay = proplists:get_value(<<"off_fan_delay">>, PostVals),
    InversionOffset = proplists:get_value(<<"inversion_offset">>, PostVals),
    %% TODO : hides the structure of Config, behind the API of e_r_config
    Config = [
        {on_pump_temp, OnPumpTemp},
        {off_pump_temp, OffPumpTemp},
        {on_pump_delay, OnPumpDelay},
        {off_pump_delay, OffPumpDelay},
        {on_fan_temp, OnFanTemp},
        {off_fan_temp, OffFanTemp},
        {on_fan_delay, OnFanDelay},
        {off_fan_delay, OffFanDelay},
        {inversion_offset, InversionOffset}
    ],
    ok = enigma_router_config:write_config(Config),
    cowboy_req:reply(200, [{<<"content-type">>, 
                            <<"text/plain; charset=utf-8">>}
                          ], [], Req2);
config(_,Req)  -> 
    %% Method not allowed
    cowboy_req:reply(405, Req).



terminate(_Reason, _Req, _State) ->
    ok.


