%%%-------------------------------------------------------------------
%%% @author manu <manu@chleb>
%%% @copyright (C) 2012, manu
%%% @doc store configuration the last 24 hours of sensor data
%%% @end
%%% @todo make sure size < 1GB (or whatever OUR limit is)
%%%       and stop writing if we reach this -- #state.full :: boolean()
%%%

%%%
%%% Created :  3 Apr 2012 by manu <manu@chleb>
%%%-------------------------------------------------------------------

-module(enigma_router_data).

-behaviour(gen_server).

-compile([{parse_transform, lager_transform}]).

%% API
-export([readings/0, readings/1, readings/2,
	 start_link/0, stations/0, write_reading/1]).

%% gen_server callbacks
-export([code_change/3, handle_call/3, handle_cast/2,
	 handle_info/2, init/1, terminate/2]).

-include("enigma_router.hrl").

-include_lib("eunit/include/eunit.hrl").

-include_lib("stdlib/include/ms_transform.hrl").

%-define(CONFIG, 'db/config.db').
%-define(SENSOR_DB, 'db/sensor.db').
-define(SENSOR_DB, ets_readings).

-define(SEC_DAY, 86400).

%%%===================================================================
%%% API
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc Starts the server
%%--------------------------------------------------------------------
-spec start_link() -> {ok, pid()} | ignore |
		      {error, any()}.

start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, [],
			  []).

%%--------------------------------------------------------------------
%% @doc Read sensor data
%%--------------------------------------------------------------------
-spec readings() -> [tuple()].

readings() -> gen_server:call(?MODULE, readings).

-spec readings(DT_Code :: integer()) -> [tuple()].

readings(DT_Code) ->
    gen_server:call(?MODULE, {readings, DT_Code}).

-spec readings(SensorId :: integer(),
	       DT_Code :: integer()) -> [tuple()].

readings(SensorId, DT_Code) ->
    gen_server:call(?MODULE, {readings, SensorId, DT_Code}).

%%--------------------------------------------------------------------
%% @doc Write sensor data
%%--------------------------------------------------------------------
-spec write_reading(readings()) -> ok.

write_reading(Data) ->
    gen_server:cast(?MODULE, {write_reading, Data}).

%%--------------------------------------------------------------------
%% @doc Returns a list of sensor stations
%%--------------------------------------------------------------------
-spec stations() -> [integer()].

stations() -> gen_server:call(?MODULE, stations).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Initializes the server
%%
%% @spec init(Args) -> {ok, State} |
%%                     {ok, State, Timeout} |
%%                     ignore |
%%                     {stop, Reason}
%% @end
%%--------------------------------------------------------------------
init([]) ->
    %%    {ok, ?CONFIG} = dets:open_file(?CONFIG, [{type, set}, {keypos, 1}]),
    %%    init_config_table(),
    %% {ok, ?SENSOR_DB} = dets:open_file(?SENSOR_DB, [{type, set}, {keypos, 1}]),
    _Tid = ets:new(?SENSOR_DB,
		   [set, {keypos, 1}, named_table]),
    {ok, no_state}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling call messages
%%
%% @spec handle_call(Request, From, State) ->
%%                                   {reply, Reply, State} |
%%                                   {reply, Reply, State, Timeout} |
%%                                   {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, Reply, State} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------

%% @doc Return all sensor data recorded in the last 24 hours
handle_call(readings, _From, State) ->
    %% Data = dets:match_object(?SENSOR_DB, '$1'),
    Data = ets:match_object(?SENSOR_DB, '$1'),
    {reply, Data, State};
%% @doc Return all DT_Code data recorded in the last 24 hours
handle_call({readings, DT_Code}, _From, State) ->
    Data = ets:match_object(?SENSOR_DB,
			    {'$1', '$2', '$3', DT_Code, '$4'}),
    Info = ets:info(?SENSOR_DB),
    {memory, Words} = proplists:lookup(memory, Info),
    KBytes = erlang:system_info(wordsize) * Words / 1024,
    lager:info("Sensor data in memory: ~f KB", [KBytes]),
    {reply, Data, State};
%% @doc Return all DT_Code data recorded for SensorId, in the last 24 hours
handle_call({readings, SensorId, DT_Code}, _From,
	    State) ->
    Data = ets:match_object(?SENSOR_DB,
			    {'$1', SensorId, '$3', DT_Code, '$4'}),
    {reply, Data, State};
%% @doc Return a list of station ids (the server)
handle_call(stations, _From, State) ->
    Data = ets:match_object(?SENSOR_DB, '$1'),
    %% inefficient might be better to keep a list of Ids in the gen_server state
    StationIds = lists:foldl(fun ({_, Id, _, _, _}, Acc) ->
				     case lists:member(Id, Acc) of
				       true -> Acc;
				       false -> [Id | Acc]
				     end
			     end,
			     [], Data),
    {reply, StationIds, State};
handle_call(_Request, _From, State) ->
    Reply = ok, {reply, Reply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling cast messages
%%
%% @spec handle_cast(Msg, State) -> {noreply, State} |
%%                                  {noreply, State, Timeout} |
%%                                  {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------

%% %% @doc Store new sensor data
handle_cast({write_reading, Data}, State) ->
    %% error_logger:info_msg("write data: ~p~n",[Data]),
    lists:foreach(fun ({SensorCode, SensorNum, DT, V}) ->
			  ets:insert(?SENSOR_DB,
				     {now(), SensorCode, SensorNum, DT, V})
		  end,
		  Data),
    delete_old_readings(),
    {noreply, State};
handle_cast(_Msg, State) -> {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling all non call/cast messages
%%
%% @spec handle_info(Info, State) -> {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------

handle_info(_Info, State) -> {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_server terminates
%% with Reason. The return value is ignored.
%%
%% @spec terminate(Reason, State) -> void()
%% @end
%%--------------------------------------------------------------------
terminate(_Reason, _State) ->
    %%    ok = dets:close(?CONFIG),
    %%    ok = dets:close(?SENSOR_DB).
    ok.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Convert process state when code is changed
%%
%% @spec code_change(OldVsn, State, Extra) -> {ok, NewState}
%% @end
%%--------------------------------------------------------------------
code_change(_OldVsn, State, _Extra) -> {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================

%% @doc maximum memory size we want to use for local sensor data DB, in number of memory words
%% @ todo adjust size to store 24 hours worth of data
%% data_max_size() ->
%%     10 * 1024. % 10K words (a data record is ??? bytes)
%%
%%
%%
%% check_size() ->
%%     MaxSize = data_max_size(),
%%     case dets:info(?SENSOR_DB, file_size) of
%%         {file_size, Size} when Size > 0.95 * MaxSize ->
%%             router_tcp:alert("Router DB is full"),
%%             full;
%%         {file_size, Size} when Size > 0.75 * MaxSize ->
%%             router_tcp:alert("Router DB is 75% full"),
%%             ok;
%%         {file_size, _Size} ->
%%             ok;
%%         Other ->
%%             error_logger:error_msg("Data dets file size: ~w~n",[Other]),
%%             ok
%%     end.

%% delete readings whose timestamp is smaller than now - 86400 sec
delete_old_readings() ->
    Now = now(),
    All = ets:match_object(?SENSOR_DB, '$1'),
    Old = lists:filter(fun ({T, _, _, _, _}) ->
			       %%day_difference(Now, T) > 1
			       time_diff(Now, T) > 86400
		       end,
		       All),
    lists:foreach(fun ({Key, _, _, _, _}) ->
			  true = ets:delete(?SENSOR_DB, Key)
		  end,
		  Old).

%% day_difference(T1, T2) ->
%%     {D1,_} = calendar:now_to_datetime(T1),
%%     {D2,_} = calendar:now_to_datetime(T2),
%%     Days1 = calendar:date_to_gregorian_days(D1),
%%     Days2 = calendar:date_to_gregorian_days(D2),
%%     Days1 - Days2.

time_diff(ET1, ET2) ->
    DT1 = calendar:now_to_datetime(ET1),
    DT2 = calendar:now_to_datetime(ET2),
    Sec1 = calendar:datetime_to_gregorian_seconds(DT1),
    Sec2 = calendar:datetime_to_gregorian_seconds(DT2),
    Sec1 - Sec2.

%% Tests for delete_old_readings here !!!

