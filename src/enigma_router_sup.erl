-module(enigma_router_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

%% Helper macro for declaring children of supervisor
-define(CHILD(I, Type), {I, {I, start_link, []}, permanent, 5000, Type, [I]}).

%% ===================================================================
%% API functions
%% ===================================================================

start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

%% ===================================================================
%% Supervisor callbacks
%% ===================================================================

init([]) ->
    Main = {enigma_router_main, {enigma_router_main, start_link, []},
            permanent, 1000, worker, [enigma_router_main]},
    Modem = {enigma_router_modem, {enigma_router_modem, start_link, []},
            permanent, 1000, worker, [enigma_router_modem]},
    Xbee = {enigma_router_xbee, {enigma_router_xbee, start_link, []},
            permanent, 1000, worker, [enigma_router_xbee]},
    Tcp = {enigma_router_tcp, {enigma_router_tcp, start_link, []},
            permanent, 1000, worker, [enigma_router_tcp]},
    Data = {enigma_router_data, {enigma_router_data, start_link, []},
            permanent, 1000, worker, [enigma_router_data]},
    Alert = {enigma_router_alert, {enigma_router_alert, start_link, []},
            permanent, 1000, worker, [enigma_router_alert]},
    Config = {enigma_router_config, {enigma_router_config, start_link, []},
            permanent, 1000, worker, [enigma_router_config]},

    {ok, {{one_for_one, 5, 10}, 
          [Main, Xbee, Tcp, Modem, Data, Alert, Config]}}.


