
%%%-------------------------------------------------------------------
%%% @author manu <manu@chleb>
%%% @copyright (C) 2013, manu
%%% @doc This module implements the application main state machine
%%%  
%%% @end
%%% Created : 30 Jul 2013 by manu <manu@chleb>
%%%-------------------------------------------------------------------
-module(enigma_router_main).

-behaviour(gen_fsm).
-compile([{parse_transform, lager_transform}]).

%% API
-export([start_link/0, 
         sensor_data/1,
         no_sensor_data/0,
         router_status/0,
         use_modem/0,
         use_tcp/0,
         start_pump/0,
         stop_pump/0,
         start_fan/0,
         stop_fan/0
        ]).

%% gen_fsm callbacks
-export([init/1, tcp/2, modem/2, 
         state_name/3, handle_event/3,
         handle_sync_event/4, handle_info/3, terminate/3, code_change/4]).

-include("enigma_router.hrl").

-define(SERVER, ?MODULE).

-record(state, {property}).


%%%===================================================================
%%% API
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Creates a gen_fsm process which calls Module:init/1 to
%% initialize. To ensure a synchronized start-up procedure, this
%% function does not return until Module:init/1 has returned.
%%
%% @spec start_link() -> {ok, Pid} | ignore | {error, Error}
%% @end
%%--------------------------------------------------------------------
start_link() ->
    gen_fsm:start_link({local, ?SERVER}, ?MODULE, [], []).

sensor_data(Data) ->
    %%lager:info("main received ~s~n",[Data]),
    gen_fsm:send_event(?SERVER, {sensor_data, Data}).

no_sensor_data() ->
    gen_fsm:send_event(?SERVER, no_sensor_data).

-spec use_modem() -> 'ok'.
use_modem() ->
    gen_fsm:send_event(?SERVER, use_modem).

-spec use_tcp() -> 'ok'.
use_tcp() ->
    gen_fsm:send_event(?SERVER, use_tcp).

router_status() ->
    gen_fsm:sync_send_all_state_event(?SERVER, router_status).

-spec start_pump() -> 'ok'.
start_pump() -> 
    ok = gpio:set(?POWER_ACT).

-spec stop_pump() -> 'ok'.

stop_pump() -> 
    ok = gpio:clr(?POWER_ACT).

-spec start_fan() -> 'ok'.
start_fan() -> 
    ok = gpio:set(?FAN_ACT).

-spec stop_fan() -> 'ok'.

stop_fan() -> 
    ok = gpio:clr(?FAN_ACT).

%%%===================================================================
%%% gen_fsm callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Whenever a gen_fsm is started using gen_fsm:start/[3,4] or
%% gen_fsm:start_link/[3,4], this function is called by the new
%% process to initialize.
%%
%% @spec init(Args) -> {ok, StateName, State} |
%%                     {ok, StateName, State, Timeout} |
%%                     ignore |
%%                     {stop, StopReason}
%% @end
%%--------------------------------------------------------------------
init([]) ->    
    {ok, Prop} = application:get_env(property),
    %% lager:info("Property: ~p~n",[Prop]),
    %% lager:info("~p~n",[application:get_all_env(gpio)]),
    {ok, tcp, #state{property=Prop}}. 

%%--------------------------------------------------------------------
%% @private
%% @doc
%% There should be one instance of this function for each possible
%% state name. Whenever a gen_fsm receives an event sent using
%% gen_fsm:send_event/2, the instance of this function with the same
%% name as the current state name StateName is called to handle
%% the event. It is also called if a timeout occurs.
%%
%% @spec state_name(Event, State) ->
%%                   {next_state, NextStateName, NextState} |
%%                   {next_state, NextStateName, NextState, Timeout} |
%%                   {stop, Reason, NewState}
%% @end
%%--------------------------------------------------------------------
tcp(use_tcp, State) ->
    lager:info("MAIN: tcp <- use_tcp~n"),
    {next_state, tcp, State};
tcp(use_modem, State) ->
    lager:info("MAIN: tcp <- use_modem~n"),
    {next_state, modem, State};
tcp(no_sensor_data, State) ->
    lager:info("MAIN: tcp <- no_sensor_data~n"),
    {next_state, tcp, State};
tcp({sensor_data, Payload}, #state{property=Prop}=State) ->
    lager:info("MAIN: tcp <- sensor_data: ~w~n",[Payload]),
    Data = parse(Payload),
    %% store parsed data into DB
    enigma_router_data:write_reading(Data),
    %% sends parsed data to alert monitor
    enigma_router_alert:sensor_data(Data),
    %% add router id + status
    NewData = add_router_data(Prop, Payload),
    lager:info("MAIN: + router data: ~w~n",[NewData]),
    %% send data
    enigma_router_tcp:sensor_data(NewData),
    {next_state, tcp, State};
tcp(Event, State) ->
    lager:info("MAIN: tcp <- ~w~n",[Event]),
    {next_state, tcp, State}.


modem(use_tcp, State) ->
    lager:info("MAIN: modem <- use_tcp~n"),
    {next_state, tcp, State};
modem(use_modem, State) ->
    lager:info("MAIN: modem <- use_modem~n"),
    {next_state, modem, State};
modem(no_sensor_data, State) ->
    lager:info("MAIN: modem <- no_sensor_data~n"),
    {next_state, modem, State};
modem({sensor_data, Payload}, #state{property=Prop}=State) ->
    %% lager:info("MAIN: sensor_data: ~w~n",Payload),
    Data = parse(Payload),
    %% store parsed data into DB
    enigma_router_data:write_reading(Data),
    %% sends parsed data to alert monitor
    enigma_router_alert:sensor_data(Data),
    %% add router id + status
    NewData = add_router_data(Prop, Payload),
    %% lager:info("MAIN: + router data: ~w~n",[NewData]),
    %% send data
    enigma_router_modem:sensor_data(NewData),
    {next_state, modem, State};
modem(Event, State) ->
    lager:info("MAIN: modem <- ~w~n",[Event]),
    {next_state, modem, State}.


%%--------------------------------------------------------------------
%% @private
%% @doc
%% There should be one instance of this function for each possible
%% state name. Whenever a gen_fsm receives an event sent using
%% gen_fsm:sync_send_event/[2,3], the instance of this function with
%% the same name as the current state name StateName is called to
%% handle the event.
%%
%% @spec state_name(Event, From, State) ->
%%                   {next_state, NextStateName, NextState} |
%%                   {next_state, NextStateName, NextState, Timeout} |
%%                   {reply, Reply, NextStateName, NextState} |
%%                   {reply, Reply, NextStateName, NextState, Timeout} |
%%                   {stop, Reason, NewState} |
%%                   {stop, Reason, Reply, NewState}
%% @end
%%--------------------------------------------------------------------
state_name(_Event, _From, State) ->
    Reply = ok,
    {reply, Reply, state_name, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Whenever a gen_fsm receives an event sent using
%% gen_fsm:send_all_state_event/2, this function is called to handle
%% the event.
%%
%% @spec handle_event(Event, StateName, State) ->
%%                   {next_state, NextStateName, NextState} |
%%                   {next_state, NextStateName, NextState, Timeout} |
%%                   {stop, Reason, NewState}
%% @end
%%--------------------------------------------------------------------
handle_event(_Event, StateName, State) ->
    {next_state, StateName, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Whenever a gen_fsm receives an event sent using
%% gen_fsm:sync_send_all_state_event/[2,3], this function is called
%% to handle the event.
%%
%% @spec handle_sync_event(Event, From, StateName, State) ->
%%                   {next_state, NextStateName, NextState} |
%%                   {next_state, NextStateName, NextState, Timeout} |
%%                   {reply, Reply, NextStateName, NextState} |
%%                   {reply, Reply, NextStateName, NextState, Timeout} |
%%                   {stop, Reason, NewState} |
%%                   {stop, Reason, Reply, NewState}
%% @end
%%--------------------------------------------------------------------
handle_sync_event(router_status, _From, StateName, State) ->
    Status = get_router_status(),
    {reply, Status, StateName, State};
handle_sync_event(_Event, _From, StateName, State) ->
    Reply = ok,
    {reply, Reply, StateName, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_fsm when it receives any
%% message other than a synchronous or asynchronous event
%% (or a system message).
%%
%% @spec handle_info(Info,StateName,State)->
%%                   {next_state, NextStateName, NextState} |
%%                   {next_state, NextStateName, NextState, Timeout} |
%%                   {stop, Reason, NewState}
%% @end
%%--------------------------------------------------------------------
handle_info(_Info, StateName, State) ->
    {next_state, StateName, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_fsm when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_fsm terminates with
%% Reason. The return value is ignored.
%%
%% @spec terminate(Reason, StateName, State) -> void()
%% @end
%%--------------------------------------------------------------------
terminate(_Reason, _StateName, _State) ->
    ok.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Convert process state when code is changed
%%
%% @spec code_change(OldVsn, StateName, State, Extra) ->
%%                   {ok, StateName, NewState}
%% @end
%%--------------------------------------------------------------------
code_change(_OldVsn, StateName, State, _Extra) ->
    {ok, StateName, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================
add_router_data(Prop, SensorData) ->
    NewData = add_router_status(SensorData),
    %%lager:info("NewData: ~p~n",[NewData]),
    add_router_id(Prop, NewData).

add_router_status(SensorData) ->
    Status = get_router_status(),
    %%lager:info("Status: ~p~n",[Status]),
    << Status/binary, SensorData/binary>>.

%% Property is an integer on 4 bytes
add_router_id(Prop, Data) ->
    %%lager:info("Prop: ~p, Data: ~p~n",[Prop,Data]),
    <<Prop:32/integer-little, Data/binary>>.

get_router_status() ->
    {ok, ATReady} = gpio:get(?AT_READY),
    {ok, PowerOn} = gpio:get(?POWER_ON),
    {ok, PowerErr} = gpio:get(?POWER_ERR),
    {ok, PowerAct} = gpio:get(?POWER_ACT),
    {ok, FanAct} = gpio:get(?FAN_ACT),
    <<0:3, ATReady:1, PowerOn:1, PowerErr:1, PowerAct:1, FanAct:1>>.



    




%% parse the binary readings to a list of
%% {SensorCode, SensorNum, DT_Code, Value / Scalar}
%% For each sensor value, retrieve the sensor scalar with get_data_type 
%% and scale the value accordingly
parse(<<SensorCode:32/integer-little, Readings/binary>>) ->
    Res = parse_readings(Readings, SensorCode, []),
    error_logger:info_msg("Parsed payload: ~p~n", [Res]),
    Res.

parse_readings(<<>>, _, _) ->
    [];
parse_readings(<<DT_Code:8, Rest/binary>>, SensorCode, SensorCount) -> 
    {ByteSize, Scalar} = get_data_type(DT_Code),                         
    lager:debug("DT_Code: ~w, ByteSize: ~w, Scalar ~w", [DT_Code, ByteSize, Scalar]),
    Bits = ByteSize * 8, 
    <<Value:Bits/little-signed-integer, Rest2/binary>> = Rest,
    lager:debug("Value: ~w, Scalar: ~w",[Value, Scalar]),
    SensorNum  = sensor_count(DT_Code, SensorCount),
    [{SensorCode, SensorNum, DT_Code, Value / Scalar}] ++ 
        parse_readings(Rest2, SensorCode, inc_sensor_count(DT_Code, SensorCount)).

%sensor_count(DT_Code, []) -> {1, inc_sensor(DT_Code, [])};
sensor_count(DT_Code, PropL) ->
    case proplists:lookup(DT_Code, PropL) of
        {DT_Code, Count} -> Count+1;
        none             -> 1
    end.

%inc_sensor(DT_Code, []) -> [{DT_Code, 1}];
inc_sensor_count(DT_Code, SensorCount) ->
    case lists:keyfind(DT_Code, 1, SensorCount) of
        false -> [{DT_Code, 1}|SensorCount];
        {DT_Code, Count} -> 
            lists:keyreplace(DT_Code, 1, SensorCount, {DT_Code, Count+1})
    end. 

 

%% gets {DT_Id, ByteSize, Scalar} from DT_Code (hardcoded for now)

%% #<DataType id: 3, data_type_code: 3, unit: "Pa", description: "Barometric pressure", created_at: "2013-09-30 05:44:10", updated_at: "2013-10-21 00:12:57", number_of_bytes: 4, scalar: 1>, 
%% #<DataType id: 1, data_type_code: 0, unit: "°C", description: "External temperature", created_at: "2013-09-30 05:44:10", updated_at: "2013-10-31 00:39:09", number_of_bytes: 2, scalar: 100>, 
%% #<DataType id: 2, data_type_code: 1, unit: "°C", description: "Internal temperature", created_at: "2013-09-30 05:44:10", updated_at: "2013-10-31 00:39:16", number_of_bytes: 2, scalar: 100>, 
%% #<DataType id: 5, data_type_code: 4, unit: "V", description: "Battery Voltage", created_at: "2013-10-21 00:15:41", updated_at: "2013-10-31 00:39:38", number_of_bytes: 2, scalar: 100>, 
%% #<DataType id: 4, data_type_code: 2, unit: "%RH", description: "Relative Humidity", created_at: "2013-10-21 00:12:14", updated_at: "2013-10-31 00:39:47", number_of_bytes: 2, scalar: 100>
get_data_type(0) -> {2, 100};
get_data_type(1) -> {2, 100};
get_data_type(2) -> {2, 100};
get_data_type(3) -> {4, 1};
get_data_type(4) -> {2, 100};
get_data_type(5) -> {2, 100}; %% Is it now the Fan temperature and not the Voltage ???
get_data_type(6) -> {2, 1}.
