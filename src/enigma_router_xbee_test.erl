

%%%-------------------------------------------------------------------
%%% @author manu <manu@chleb>
%%% @copyright (C) 2013, manu
%%% @doc a version of xbee module to run tests in the absence of an xbee
%%%  
%%% @end
%%% Created : 30 Jul 2013 by manu <manu@chleb>
%%%-------------------------------------------------------------------
-module(enigma_router_xbee_test).

-behaviour(gen_fsm).

-include_lib("eunit/include/eunit.hrl").

%% API
-export([start_link/0, check/2]).

%% gen_fsm callbacks
-export([init/1, start/2, length/2, payload/2, state_name/3, handle_event/3,
         handle_sync_event/4, handle_info/3, terminate/3, code_change/4]).

-define(SERVER, ?MODULE).
-define(DEVICE, "/dev/ttyUSB0").

-define(NO_RECEPTION, 5 * 60 * 1000). %% 5 mn

-record(state, {buffer, length}).

%%%===================================================================
%%% API
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Creates a gen_fsm process which calls Module:init/1 to
%% initialize. To ensure a synchronized start-up procedure, this
%% function does not return until Module:init/1 has returned.
%%
%% @spec start_link() -> {ok, Pid} | ignore | {error, Error}
%% @end
%%--------------------------------------------------------------------
start_link() ->
    gen_fsm:start_link({local, ?SERVER}, ?MODULE, [], []).

%%%===================================================================
%%% gen_fsm callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Whenever a gen_fsm is started using gen_fsm:start/[3,4] or
%% gen_fsm:start_link/[3,4], this function is called by the new
%% process to initialize.
%%
%% @spec init(Args) -> {ok, StateName, State} |
%%                     {ok, StateName, State, Timeout} |
%%                     ignore |
%%                     {stop, StopReason}
%% @end
%%--------------------------------------------------------------------
init([]) ->
    {ok, start, #state{buffer= <<>>, length=undefined}}. 

%%--------------------------------------------------------------------
%% @private
%% @doc
 
%% There should be one instance of this function for each possible
%% state name. Whenever a gen_fsm receives an event sent using
%% gen_fsm:send_event/2, the instance of this function with the same
%% name as the current state name StateName is called to handle
%% the event. It is also called if a timeout occurs.
%%
%% @spec state_name(Event, State) ->
%%                   {next_state, NextStateName, NextState} |
%%                   {next_state, NextStateName, NextState, Timeout} |
%%                   {stop, Reason, NewState}
%% @end
%%--------------------------------------------------------------------

%% transition to 'length' when first byte is 126 (0x7E)
%% buffer is empty
start({data, <<16#7E>>}, S) -> 
    io:format("START~n"),
    {next_state, length, S#state{buffer= <<>>}};  
%% discard other messages
start(_Event, State) ->
    {next_state, start, State}.


%% when buffer is empty, next byte is MSB
length({data, MSB}, #state{buffer= <<>>}=S) ->
    io:format("MSB~n"),
    {next_state, length, S#state{buffer=MSB}};
%% when buffer holds MSB, next byte is LSB,
%% we calculate length and transition to 'payload'
length({data, LSB}, #state{buffer=MSB}=S) -> 
    io:format("LSB~n"),
    <<Length:16>> = <<MSB/binary, LSB/binary>>,
    io:format("Length~p~n",[Length]),
    {next_state, payload, S#state{length=Length, buffer= <<>>}};
%% discard other messages
length(_Event, State) ->
    {next_state, length, State}.


%% If Buffer size == Length, next byte is the checksum
%% We check the checksum and send to enigma_router_main
payload({data, <<CheckSum>>}, #state{length=L, buffer=B}=S) 
  when byte_size(B) == L ->
    io:format("Buffer ~p~n",[B]),
    forward(check(CheckSum, B), B), %% check the checksum and send
    {next_state, start, S#state{buffer= <<>>, length=undefined}};
%% if Buffer size < Length, we add next byte to the buffer
payload({data, Byte}, #state{buffer=B}=S) ->
    io:format("Buffering... ~n"),
    {next_state, payload, S#state{buffer= <<B/binary, Byte/binary>>}};
%% discard other messages
payload(_Event, State) ->
    {next_state, payload, State}.


%%--------------------------------------------------------------------
%% @private
%% @doc
%% There should be one instance of this function for each possible
%% state name. Whenever a gen_fsm receives an event sent using
%% gen_fsm:sync_send_event/[2,3], the instance of this function with
%% the same name as the current state name StateName is called to
%% handle the event.
%%
%% @spec state_name(Event, From, State) ->
%%                   {next_state, NextStateName, NextState} |
%%                   {next_state, NextStateName, NextState, Timeout} |
%%                   {reply, Reply, NextStateName, NextState} |
%%                   {reply, Reply, NextStateName, NextState, Timeout} |
%%                   {stop, Reason, NewState} |
%%                   {stop, Reason, Reply, NewState}
%% @end
%%--------------------------------------------------------------------
state_name(_Event, _From, State) ->
    Reply = ok,
    {reply, Reply, state_name, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Whenever a gen_fsm receives an event sent using
%% gen_fsm:send_all_state_event/2, this function is called to handle
%% the event.
%%
%% @spec handle_event(Event, StateName, State) ->
%%                   {next_state, NextStateName, NextState} |
%%                   {next_state, NextStateName, NextState, Timeout} |
%%                   {stop, Reason, NewState}
%% @end
%%--------------------------------------------------------------------
handle_event(_Event, StateName, State) ->
    {next_state, StateName, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Whenever a gen_fsm receives an event sent using
%% gen_fsm:sync_send_all_state_event/[2,3], this function is called
%% to handle the event.
%%
%% @spec handle_sync_event(Event, From, StateName, State) ->
%%                   {next_state, NextStateName, NextState} |
%%                   {next_state, NextStateName, NextState, Timeout} |
%%                   {reply, Reply, NextStateName, NextState} |
%%                   {reply, Reply, NextStateName, NextState, Timeout} |
%%                   {stop, Reason, NewState} |
%%                   {stop, Reason, Reply, NewState}
%% @end
%%--------------------------------------------------------------------
handle_sync_event(_Event, _From, StateName, State) ->
    Reply = ok,
    {reply, Reply, StateName, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_fsm when it receives any
%% message other than a synchronous or asynchronous event
%% (or a system message).
%%
%% @spec handle_info(Info,StateName,State)->
%%                   {next_state, NextStateName, NextState} |
%%                   {next_state, NextStateName, NextState, Timeout} |
%%                   {stop, Reason, NewState}
%% @end
%%--------------------------------------------------------------------
%% when we received a bunch of bytes from the serial, 
%% we stream each byte as a series of events 
%% This allows the fsm to deal with a single byte at a time
handle_info({data, Bytes}, StateName, State) ->
    lists:foreach(
      fun(Byte) -> gen_fsm:send_event(self(), {data, <<Byte>>}) end,
      binary:bin_to_list(Bytes)),
    {next_state, StateName, State};
handle_info(_Info, StateName, State) ->
    {next_state, StateName, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_fsm when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_fsm terminates with
%% Reason. The return value is ignored.
%%
%% @spec terminate(Reason, StateName, State) -> void()
%% @end
%%--------------------------------------------------------------------
terminate(_Reason, _StateName, State) ->
    ok.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Convert process state when code is changed
%%
%% @spec code_change(OldVsn, StateName, State, Extra) ->
%%                   {ok, StateName, NewState}
%% @end
%%--------------------------------------------------------------------
code_change(_OldVsn, StateName, State, _Extra) ->
    {ok, StateName, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================
check(CheckSum, Bytes) ->
    ByteList = binary:bin_to_list(Bytes),
    <<Sum>> = lists:foldl(fun(X,<<Y>>) -> <<(X+Y)>> end, <<0>>, ByteList),
    Res = (16#FF - Sum) == CheckSum,
    io:format("Checksum ~p~n",[Res]),
    Res.


forward(true, Bytes) ->
    %% extract payload (from byte 14) 
    Payload = binary:part(Bytes, 14, byte_size(Bytes) - 14),
    ?debugFmt("Payload ~w~n",[Payload]),
    Payload;
forward(false, _) ->
    do_nothing.


%% @todo turn into a eunit test
test_bytes() -> 
    Bytes = <<16#10, 16#01, 16#00, 16#13, 16#A2, 16#00, 16#40, 16#0A, 
              16#01, 16#27, 16#FF, 16#FE, 16#00, 16#00, 16#54, 16#78, 
              16#44, 16#61, 16#74, 16#61, 16#30, 16#41>>,
    CheckSum = 16#13,
    {ok, _} = check(CheckSum, Bytes).


%%%===================================================================
%%% Messages to serial port
%%%===================================================================
% outgoing messages: SerialPort ! Msg
% {send, Bytes} 
% {connect} 
% {disconnect} 
% {open, TTY} 
% {close} 
% {speed, NewInSpeed, NewOutSpeed} 
% {speed, NewSpeed} 
% {parity_odd} 
% {parity_even} 
% {break} 
% stop
%
% incoming messages
% {data, Bytes}
% {'EXIT', SerialPort, Why} 


