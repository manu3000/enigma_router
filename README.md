## enigma_router_app

* configure web server routes and port
* start the top supervisor enigma_router_sup
* init the GPIO

## alert_config_handler

allows to read/edit the alerts/actions configuration via HTTP (port: 8888)

## ws_handler

return sensor readings and stations ids, via websocket (url: /websocket) 
also regularly sends config, router status, signal strength and external temperature

## enigma_router_sup

Start the following processes in this order:

* main (enigma_router_main)
* modem (enigma_router_modem)
* xbee (enigma_router_xbee)
* tcp (enigma_router_tcp)
* data (enigma_router_data)
* alert (enigma_router_alert)
* config (enigma_router_config)

All these are handled and restarted independently of each other (they do not depend on each other)

## enigma_router_main

This module implements the application main state machine (tcp state | modem state)

## enigma_router_modem

State machine to handle sending and receiving messages via modem

## enigma_router_xbee

Finite State Machine that parses a stream of bytes (from serial port) into data packets which are then sent to enigma_router_main 

## enigma_router_tcp

This module opens a TCP socket and send sensor data. It also routinely checks that a TCP connection can be established. If opening the socket fails, tells enigma_router_main to use modem. If it succeeds, tells it to use tcp. 

## enigma_router_data

in memory database that keeps the last 24 hours of sensor data

## enigma_router_alert

This module is a server which monitors sensor data (proxied via enigma_router_main) and raise alerts if needs be. The server state keeps track of previous sensor readings. The alerts settings are obtained from enigma_router_config.

## enigma_router_config

On-disk database storing the router config, allows read/write

## sensor data

This module defines functions to help manipulate sensor data
